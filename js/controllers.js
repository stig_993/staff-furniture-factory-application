/**
 * INSPINIA - Responsive Admin Theme
 *
 * Main controller.js file
 * Define controllers with data used in Inspinia theme
 *
 *
 * Functions (controllers)
 *  - MainCtrl
 *  - dashboardFlotOne
 *  - dashboardFlotTwo
 *  - dashboardFlotFive
 *  - dashboardMap
 *  - flotChartCtrl
 *  - rickshawChartCtrl
 *  - sparklineChartCtrl
 *  - widgetFlotChart
 *  - modalDemoCtrl
 *  - ionSlider
 *  - wizardCtrl
 *  - CalendarCtrl
 *  - chartJsCtrl
 *  - GoogleMaps
 *  - ngGridCtrl
 *  - codeEditorCtrl
 *  - nestableCtrl
 *  - notifyCtrl
 *  - translateCtrl
 *  - imageCrop
 *  - diff
 *  - idleTimer
 *  - liveFavicon
 *  - formValidation
 *  - agileBoard
 *  - draggablePanels
 *  - chartistCtrl
 *  - metricsCtrl
 *  - sweetAlertCtrl
 *  - selectCtrl
 *  - toastrCtrl
 *  - loadingCtrl
 *  - datatablesCtrl
 *  - truncateCtrl
 *  - touchspinCtrl
 *  - tourCtrl
 *  - jstreeCtrl
 *
 *
 */

/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl() {
  /**
   * daterange - Used as initial model for data range picker in Advanced form view
   */
  this.daterange = {startDate: null, endDate: null};

  // $rootScope.$on("$stateChangeStart", function(){
  //   console.log(localStorage.getItem('token'));
  //  // if (localStorage.getItem('token') == undefined) {
  //  //   $state.go('login')
  //  // }
  // });

  /**
   * slideInterval - Interval for bootstrap Carousel, in milliseconds:
   */
  this.slideInterval = 5000;


  /**
   * states - Data used in Advanced Form view for Chosen plugin
   */
  this.states = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming'
  ];

  /**
   * check's - Few variables for checkbox input used in iCheck plugin. Only for demo purpose
   */
  this.checkOne = true;
  this.checkTwo = true;
  this.checkThree = true;
  this.checkFour = true;

  /**
   * knobs - Few variables for knob plugin used in Advanced Plugins view
   */
  this.knobOne = 75;
  this.knobTwo = 25;
  this.knobThree = 50;

  /**
   * Variables used for Ui Elements view
   */
  this.bigTotalItems = 175;
  this.bigCurrentPage = 1;
  this.maxSize = 5;
  this.singleModel = false;
  this.radioModel = 'Middle';
  this.checkModel = {
    left: false,
    middle: true,
    right: false
  };

  /**
   * groups - used for Collapse panels in Tabs and Panels view
   */
  this.groups = [{
    title: 'Dynamic Group Header - 1',
    content: 'Dynamic Group Body - 1'
  },
    {
      title: 'Dynamic Group Header - 2',
      content: 'Dynamic Group Body - 2'
    }
  ];

  /**
   * alerts - used for dynamic alerts in Notifications and Tooltips view
   */
  this.alerts = [
    {type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.'},
    {type: 'success', msg: 'Well done! You successfully read this important alert message.'},
    {type: 'info', msg: 'OK, You are done a great job man.'}
  ];

  /**
   * addAlert, closeAlert  - used to manage alerts in Notifications and Tooltips view
   */
  this.addAlert = function () {
    this.alerts.push({msg: 'Another alert!'});
  };

  this.closeAlert = function (index) {
    this.alerts.splice(index, 1);
  };

  /**
   * randomStacked - used for progress bar (stacked type) in Badges adn Labels view
   */
  this.randomStacked = function () {
    this.stacked = [];
    var types = ['success', 'info', 'warning', 'danger'];

    for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
      var index = Math.floor((Math.random() * 4));
      this.stacked.push({
        value: Math.floor((Math.random() * 30) + 1),
        type: types[index]
      });
    }
  };
  /**
   * initial run for random stacked value
   */
  this.randomStacked();

  /**
   * summernoteText - used for Summernote plugin
   */
  this.summernoteText = ['<h3>Hello Jonathan! </h3>',
    '<p>dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the dustrys</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more',
    'recently with</p>'
  ].join('');

  /**
   * General variables for Peity Charts
   * used in many view so this is in Main controller
   */
  this.BarChart = {
    data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2, 4, 7, 3, 2, 7, 9, 6, 4, 5, 7, 3, 2, 1, 0, 9, 5, 6, 8, 3, 2, 1],
    options: {
      fill: ["#1ab394", "#d7d7d7"],
      width: 100
    }
  };

  this.BarChart2 = {
    data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };

  this.BarChart3 = {
    data: [5, 3, 2, -1, -3, -2, 2, 3, 5, 2],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };

  this.LineChart = {
    data: [5, 9, 7, 3, 5, 2, 5, 3, 9, 6, 5, 9, 4, 7, 3, 2, 9, 8, 7, 4, 5, 1, 2, 9, 5, 4, 7],
    options: {
      fill: '#1ab394',
      stroke: '#169c81',
      width: 64
    }
  };

  this.LineChart2 = {
    data: [3, 2, 9, 8, 47, 4, 5, 1, 2, 9, 5, 4, 7],
    options: {
      fill: '#1ab394',
      stroke: '#169c81',
      width: 64
    }
  };

  this.LineChart3 = {
    data: [5, 3, 2, -1, -3, -2, 2, 3, 5, 2],
    options: {
      fill: '#1ab394',
      stroke: '#169c81',
      width: 64
    }
  };

  this.LineChart4 = {
    data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
    options: {
      fill: '#1ab394',
      stroke: '#169c81',
      width: 64
    }
  };

  this.PieChart = {
    data: [1, 5],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };

  this.PieChart2 = {
    data: [226, 360],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };
  this.PieChart3 = {
    data: [0.52, 1.561],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };
  this.PieChart4 = {
    data: [1, 4],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };
  this.PieChart5 = {
    data: [226, 134],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };
  this.PieChart6 = {
    data: [0.52, 1.041],
    options: {
      fill: ["#1ab394", "#d7d7d7"]
    }
  };
};


/**
 * dashboardFlotOne - simple controller for data
 * for Flot chart in Dashboard view
 */
function dashboardFlotOne() {

  var data1 = [
    [0, 4],
    [1, 8],
    [2, 5],
    [3, 10],
    [4, 4],
    [5, 16],
    [6, 5],
    [7, 11],
    [8, 6],
    [9, 11],
    [10, 30],
    [11, 10],
    [12, 13],
    [13, 4],
    [14, 3],
    [15, 3],
    [16, 6]
  ];
  var data2 = [
    [0, 1],
    [1, 0],
    [2, 2],
    [3, 0],
    [4, 1],
    [5, 3],
    [6, 1],
    [7, 5],
    [8, 2],
    [9, 3],
    [10, 2],
    [11, 1],
    [12, 0],
    [13, 2],
    [14, 8],
    [15, 0],
    [16, 0]
  ];

  var options = {
    series: {
      lines: {
        show: false,
        fill: true
      },
      splines: {
        show: true,
        tension: 0.4,
        lineWidth: 1,
        fill: 0.4
      },
      points: {
        radius: 0,
        show: true
      },
      shadowSize: 2,
      grow: {stepMode: "linear", stepDirection: "up", steps: 80}
    },
    grow: {stepMode: "linear", stepDirection: "up", steps: 80},
    grid: {
      hoverable: true,
      clickable: true,
      tickColor: "#d5d5d5",
      borderWidth: 1,
      color: '#d5d5d5'
    },
    colors: ["#1ab394", "#1C84C6"],
    xaxis: {},
    yaxis: {
      ticks: 4
    },
    tooltip: false
  };

  /**
   * Definition of variables
   * Flot chart
   */
  this.flotData = [data1, data2];
  this.flotOptions = options;
}

/**
 * dashboardFlotTwo - simple controller for data
 * for Flot chart in Dashboard view
 */
function dashboardFlotTwo() {

  var data1 = [
    [gd(2012, 1, 1), 7],
    [gd(2012, 1, 2), 6],
    [gd(2012, 1, 3), 4],
    [gd(2012, 1, 4), 8],
    [gd(2012, 1, 5), 9],
    [gd(2012, 1, 6), 7],
    [gd(2012, 1, 7), 5],
    [gd(2012, 1, 8), 4],
    [gd(2012, 1, 9), 7],
    [gd(2012, 1, 10), 8],
    [gd(2012, 1, 11), 9],
    [gd(2012, 1, 12), 6],
    [gd(2012, 1, 13), 4],
    [gd(2012, 1, 14), 5],
    [gd(2012, 1, 15), 11],
    [gd(2012, 1, 16), 8],
    [gd(2012, 1, 17), 8],
    [gd(2012, 1, 18), 11],
    [gd(2012, 1, 19), 11],
    [gd(2012, 1, 20), 6],
    [gd(2012, 1, 21), 6],
    [gd(2012, 1, 22), 8],
    [gd(2012, 1, 23), 11],
    [gd(2012, 1, 24), 13],
    [gd(2012, 1, 25), 7],
    [gd(2012, 1, 26), 9],
    [gd(2012, 1, 27), 9],
    [gd(2012, 1, 28), 8],
    [gd(2012, 1, 29), 5],
    [gd(2012, 1, 30), 8],
    [gd(2012, 1, 31), 25]
  ];

  var data2 = [
    [gd(2012, 1, 1), 800],
    [gd(2012, 1, 2), 500],
    [gd(2012, 1, 3), 600],
    [gd(2012, 1, 4), 700],
    [gd(2012, 1, 5), 500],
    [gd(2012, 1, 6), 456],
    [gd(2012, 1, 7), 800],
    [gd(2012, 1, 8), 589],
    [gd(2012, 1, 9), 467],
    [gd(2012, 1, 10), 876],
    [gd(2012, 1, 11), 689],
    [gd(2012, 1, 12), 700],
    [gd(2012, 1, 13), 500],
    [gd(2012, 1, 14), 600],
    [gd(2012, 1, 15), 700],
    [gd(2012, 1, 16), 786],
    [gd(2012, 1, 17), 345],
    [gd(2012, 1, 18), 888],
    [gd(2012, 1, 19), 888],
    [gd(2012, 1, 20), 888],
    [gd(2012, 1, 21), 987],
    [gd(2012, 1, 22), 444],
    [gd(2012, 1, 23), 999],
    [gd(2012, 1, 24), 567],
    [gd(2012, 1, 25), 786],
    [gd(2012, 1, 26), 666],
    [gd(2012, 1, 27), 888],
    [gd(2012, 1, 28), 900],
    [gd(2012, 1, 29), 178],
    [gd(2012, 1, 30), 555],
    [gd(2012, 1, 31), 993]
  ];


  var dataset = [{
    label: "Number of orders",
    grow: {stepMode: "linear"},
    data: data2,
    color: "#1ab394",
    bars: {
      show: true,
      align: "center",
      barWidth: 24 * 60 * 60 * 600,
      lineWidth: 0
    }

  },
    {
      label: "Payments",
      grow: {stepMode: "linear"},
      data: data1,
      yaxis: 2,
      color: "#1C84C6",
      lines: {
        lineWidth: 1,
        show: true,
        fill: true,
        fillColor: {
          colors: [{
            opacity: 0.2
          },
            {
              opacity: 0.2
            }
          ]
        }
      }
    }
  ];


  var options = {
    grid: {
      hoverable: true,
      clickable: true,
      tickColor: "#d5d5d5",
      borderWidth: 0,
      color: '#d5d5d5'
    },
    colors: ["#1ab394", "#464f88"],
    tooltip: true,
    xaxis: {
      mode: "time",
      tickSize: [3, "day"],
      tickLength: 0,
      axisLabel: "Date",
      axisLabelUseCanvas: true,
      axisLabelFontSizePixels: 12,
      axisLabelFontFamily: 'Arial',
      axisLabelPadding: 10,
      color: "#d5d5d5"
    },
    yaxes: [{
      position: "left",
      max: 1070,
      color: "#d5d5d5",
      axisLabelUseCanvas: true,
      axisLabelFontSizePixels: 12,
      axisLabelFontFamily: 'Arial',
      axisLabelPadding: 3
    },
      {
        position: "right",
        color: "#d5d5d5",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: ' Arial',
        axisLabelPadding: 67
      }
    ],
    legend: {
      noColumns: 1,
      labelBoxBorderColor: "#d5d5d5",
      position: "nw"
    }

  };

  function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
  }

  /**
   * Definition of variables
   * Flot chart
   */
  this.flotData = dataset;
  this.flotOptions = options;
}

/**
 * dashboardFlotFive - simple controller for data
 * for Flot chart in Dashboard view
 */
function dashboardFive() {

  var data1 = [
    [0, 4],
    [1, 8],
    [2, 5],
    [3, 10],
    [4, 4],
    [5, 16],
    [6, 5],
    [7, 11],
    [8, 6],
    [9, 11],
    [10, 20],
    [11, 10],
    [12, 13],
    [13, 4],
    [14, 7],
    [15, 8],
    [16, 12]
  ];
  var data2 = [
    [0, 0],
    [1, 2],
    [2, 7],
    [3, 4],
    [4, 11],
    [5, 4],
    [6, 2],
    [7, 5],
    [8, 11],
    [9, 5],
    [10, 4],
    [11, 1],
    [12, 5],
    [13, 2],
    [14, 5],
    [15, 2],
    [16, 0]
  ];

  var options = {
    series: {
      lines: {
        show: false,
        fill: true
      },
      splines: {
        show: true,
        tension: 0.4,
        lineWidth: 1,
        fill: 0.4
      },
      points: {
        radius: 0,
        show: true
      },
      shadowSize: 2
    },
    grid: {
      hoverable: true,
      clickable: true,

      borderWidth: 2,
      color: 'transparent'
    },
    colors: ["#1ab394", "#1C84C6"],
    xaxis: {},
    yaxis: {},
    tooltip: false
  };

  /**
   * Definition of variables
   * Flot chart
   */
  this.flotData = [data1, data2];
  this.flotOptions = options;


  var sparkline1Data = [34, 43, 43, 35, 44, 32, 44, 52];
  var sparkline1Options = {
    type: 'line',
    width: '100%',
    height: '50',
    lineColor: '#1ab394',
    fillColor: "transparent"
  };

  var sparkline2Data = [32, 11, 25, 37, 41, 32, 34, 42];
  var sparkline2Options = {
    type: 'line',
    width: '100%',
    height: '50',
    lineColor: '#1ab394',
    fillColor: "transparent"
  };

  this.sparkline1 = sparkline1Data;
  this.sparkline1Options = sparkline1Options;
  this.sparkline2 = sparkline2Data;
  this.sparkline2Options = sparkline2Options;

}


/**
 * nestableCtrl - Controller for nestable list
 */
function nestableCtrl($scope) {
  $scope.remove = function (scope) {
    scope.remove();
  };

  $scope.toggle = function (scope) {
    scope.toggle();
  };

  $scope.moveLastToTheBeginning = function () {
    var a = $scope.data.pop();
    $scope.data.splice(0, 0, a);
  };

  $scope.newSubItem = function (scope) {
    var nodeData = scope.$modelValue;
    nodeData.nodes.push({
      id: nodeData.id * 10 + nodeData.nodes.length,
      title: nodeData.title + '.' + (nodeData.nodes.length + 1),
      nodes: []
    });
  };
  $scope.collapseAll = function () {
    $scope.$broadcast('collapseAll');
  };
  $scope.expandAll = function () {
    $scope.$broadcast('expandAll');
  };
  $scope.data = [{
    "id": 1,
    "title": "node1",
    "nodes": [{
      "id": 11,
      "title": "node1.1",
      "nodes": [{
        "id": 111,
        "title": "node1.1.1",
        "nodes": []
      }]
    },
      {
        "id": 12,
        "title": "node1.2",
        "nodes": []
      }
    ]
  }, {
    "id": 2,
    "title": "node2",
    "nodes": [{
      "id": 21,
      "title": "node2.1",
      "nodes": []
    },
      {
        "id": 22,
        "title": "node2.2",
        "nodes": []
      }
    ]
  }, {
    "id": 3,
    "title": "node3",
    "nodes": [{
      "id": 31,
      "title": "node3.1",
      "nodes": []
    }]
  }];
}

/**
 * codeEditorCtrl - Controller for code editor - Code Mirror
 */
// function codeEditorCtrl($scope) {
//     $scope.editorOptions = {
//         lineNumbers: true,
//         matchBrackets: true,
//         styleActiveLine: true,
//         theme: "ambiance"
//     };
//
//     $scope.editorOptions2 = {
//         lineNumbers: true,
//         matchBrackets: true,
//         styleActiveLine: true
//     };
//
// }

/**
 * ngGridCtrl - Controller for code ngGrid
 */
function ngGridCtrl($scope) {
  $scope.ngData = [
    {Name: "Moroni", Age: 50, Position: 'PR Menager', Status: 'active', Date: '12.12.2014'},
    {Name: "Teancum", Age: 43, Position: 'CEO/CFO', Status: 'deactive', Date: '10.10.2014'},
    {Name: "Jacob", Age: 27, Position: 'UI Designer', Status: 'active', Date: '09.11.2013'},
    {Name: "Nephi", Age: 29, Position: 'Java programmer', Status: 'deactive', Date: '22.10.2014'},
    {Name: "Joseph", Age: 22, Position: 'Marketing manager', Status: 'active', Date: '24.08.2013'},
    {Name: "Monica", Age: 43, Position: 'President', Status: 'active', Date: '11.12.2014'},
    {Name: "Arnold", Age: 12, Position: 'CEO', Status: 'active', Date: '07.10.2013'},
    {Name: "Mark", Age: 54, Position: 'Analyst', Status: 'deactive', Date: '03.03.2014'},
    {Name: "Amelia", Age: 33, Position: 'Sales manager', Status: 'deactive', Date: '26.09.2013'},
    {Name: "Jesica", Age: 41, Position: 'Ruby programmer', Status: 'active', Date: '22.12.2013'},
    {Name: "John", Age: 48, Position: 'Marketing manager', Status: 'deactive', Date: '09.10.2014'},
    {Name: "Berg", Age: 19, Position: 'UI/UX Designer', Status: 'active', Date: '12.11.2013'}
  ];

  $scope.ngOptions = {data: 'ngData'};
  $scope.ngOptions2 = {
    data: 'ngData',
    showGroupPanel: true,
    jqueryUIDraggable: true
  };
}

/**
 * translateCtrl - Controller for translate
 */
function translateCtrl($translate, $scope) {
  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
    $scope.language = langKey;
  };
}

function sweetAlertCtrl($scope, SweetAlert) {

  $scope.demo1 = function () {
    SweetAlert.swal({
      title: "Welcome in Alerts",
      text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    });
  }

  $scope.demo2 = function () {
    SweetAlert.swal({
      title: "Good job!",
      text: "You clicked the button!",
      type: "success"
    });
  }

  $scope.demo3 = function () {
    SweetAlert.swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function () {
        SweetAlert.swal("Ok!");
      });
  }

  $scope.demo4 = function () {
    SweetAlert.swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function (isConfirm) {
        if (isConfirm) {
          SweetAlert.swal("Deleted!", "Your imaginary file has been deleted.", "success");
        } else {
          SweetAlert.swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
  }

}

function toastrCtrl($scope, toaster) {

  $scope.demo1 = function () {
    toaster.success({body: "Hi, welcome to Inspinia. This is example of Toastr notification box."});
  };

  $scope.demo2 = function () {
    toaster.warning({title: "Title example", body: "This is example of Toastr notification box."});
  };

  $scope.demo3 = function () {
    toaster.pop({
      type: 'info',
      title: 'Title example',
      body: 'This is example of Toastr notification box.',
      showCloseButton: true

    });
  };

  $scope.demo4 = function () {
    toaster.pop({
      type: 'error',
      title: 'Title example',
      body: 'This is example of Toastr notification box.',
      showCloseButton: true,
      timeout: 600
    });
  };

}


// My Code ////////////////

function dodajKupcaController($scope, $rootScope, $http, $state, $ngConfirm) {
  $scope.appendNum = 1;

  $scope.brTelefona = [
    {telefon: "/", opis: "/"},
    {telefon: "/", opis: "/"}
  ]
  //
  // $scope.racun = [
  //   {banka: "", account: ""},
  //   {banka: "", account: ""},
  //   {banka: "", account: ""}
  // ]

  $scope.racun = [];
  $scope.addAccount = function () {
    $scope.racun.push({banka: "", account: ""})
  };

  $scope.kontakt = [];
  $scope.addKontakt = function () {
    $scope.kontakt.push({ime: "", funkcija: "", telefon: "", email: ""})
  }

  $scope.mesto = [];
  $scope.addMesto = function () {
    $scope.mesto.push({adresa: "", mesto: ""})
  }

  $scope.kupac = {
    ime: "",
    adresa: "",
    kontakt_telefon: $scope.brTelefona,
    fax: "",
    email: "",
    web_adresa: "",
    mesto: "",
    postanski_broj: "",
    pib: "",
    jib: "",
    br_racun: $scope.racun,
    kontakt_osoba: $scope.kontakt,
    mesto_isporuke: $scope.mesto
  }

  $scope.getClients = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/clients"
    }).then(function (res) {
      $scope.klijenti = res.data;
    })
  }
  $scope.getClients();

  $scope.deleteKontakt = function (index) {
    $scope.kontakt.splice(index, 1);
  }

  $scope.deleteMesto = function (index) {
    $scope.mesto.splice(index, 1);
  }

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje klijenta',
      content: 'Da li ste sigurni da želite obrisati klijenta?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/clients/" + id
            }).then(function () {
              $state.reload();
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.edit = function (id) {
    $state.go('app.azurirajKupca', {id: id});
  }

  $scope.dodajKupca = function (valid) {
    if (valid) {
      $http({
        method: "POST",
        data: $scope.kupac,
        url: $rootScope.url + 'api/clients'
      }).then(function () {
        $state.go('app.kupci');
      });
    }
  };
}

function azurirajKupcaController($scope, $http, $rootScope, $stateParams, $state) {

  $scope.kontakt = [
    {ime: "", funkcija: "", telefon: "", email: ""}
  ];

  $scope.mesto = [
    {adresa: "", mesto: ""}
  ];

  $scope.brTelefona = [
    {telefon: "", opis: ""},
    {telefon: "", opis: ""},
  ]

  $scope.racun = [
    {banka: "", account: ""},
    {banka: "", account: ""},
    {banka: "", account: ""}
  ]

  $scope.addKontakt = function () {
    $scope.kontakt.push({ime: "", funkcija: "", telefon: "", email: ""})
  }
  $scope.addMesto = function () {
    $scope.mesto.push({adresa: "", mesto: ""})
  }

  // $scope.deleteKontakt = function(index) {
  //     $scope.kontakt.splice(index, 1);
  // }
  //
  // $scope.deleteMesto = function(index) {
  //     $scope.mesto.splice(index, 1);
  // }

  $scope.kupac = {
    ime: "",
    adresa: "",
    kontakt_telefon: $scope.brTelefona,
    fax: "",
    email: "",
    web_adresa: "",
    mesto: "",
    postanski_broj: "",
    pib: "",
    jib: "",
    br_racun: $scope.racun,
    kontakt_osoba: $scope.kontakt,
    mesto_isporuke: $scope.mesto
  }

  $http({
    method: "GET",
    url: $rootScope.url + "api/clients/" + $stateParams.id
  }).then(function (res) {
    $scope.kontakt = [];
    for (var i = 0; i < res.data.kontakt_osoba.length; i++) {
      $scope.kontakt.push({
        id: res.data.kontakt_osoba[i].id,
        ime: res.data.kontakt_osoba[i].ime,
        funkcija: res.data.kontakt_osoba[i].funkcija,
        telefon: res.data.kontakt_osoba[i].telefon,
        email: res.data.kontakt_osoba[i].email,
      });
    }
    $scope.brTelefona = [];
    for (var i = 0; i < res.data.kontakt_telefon.length; i++) {
      $scope.brTelefona.push({
        id: res.data.kontakt_telefon[i].id,
        telefon: res.data.kontakt_telefon[i].telefon,
        opis: res.data.kontakt_telefon[i].opis,
      });
    }

    $scope.racun = [];
    for (var i = 0; i < res.data.br_racun.length; i++) {
      $scope.racun.push({
        id: res.data.br_racun[i].id,
        account: res.data.br_racun[i].account,
        banka: res.data.br_racun[i].banka
      });
    }

    $scope.mesto = [];
    for (var i = 0; i < res.data.mesto_isporuke.length; i++) {
      $scope.mesto.push({
        id: res.data.mesto_isporuke[i].id,
        adresa: res.data.mesto_isporuke[i].adresa,
        mesto: res.data.mesto_isporuke[i].mesto
      });
    }

    $scope.kupac = {
      adresa: res.data.adresa,
      email: res.data.email,
      fax: res.data.fax,
      ime: res.data.ime,
      mesto: res.data.mesto,
      pib: res.data.pib,
      jib: res.data.jib,
      postanski_broj: res.data.postanski_broj,
      web_adresa: res.data.web_adresa,
      mesto_isporuke: $scope.mesto,
      kontakt_telefon: $scope.brTelefona,
      kontakt_osoba: $scope.kontakt,
      br_racun: $scope.racun
    };
  })

  $scope.cancel = function () {
    $state.go('app.kupci');
  }

  $scope.deleteKontakt = function (index, id) {
    if (id == undefined) {
      $scope.kontakt.splice(index, 1);
    }
    if (id != undefined) {
      $http({
        method: "DELETE",
        url: $rootScope.url + "api/deletecontact/" + id
      }).then(function () {
        $scope.kontakt.splice(index, 1);
      })
    }
  }

  $scope.deleteMesto = function (index, id) {
    if (id == undefined) {
      $scope.mesto.splice(index, 1);
    }
    if (id != undefined) {
      $http({
        method: "DELETE",
        url: $rootScope.url + "api/deleteaddress/" + id
      }).then(function () {
        $scope.mesto.splice(index, 1);
      })
    }
  }


  $scope.azurirajKupca = function () {
    $http({
      method: "PUT",
      url: $rootScope.url + "api/clients/" + $stateParams.id,
      data: $scope.kupac
    }).then(function () {
      $state.go('app.kupci');
    })
  }
}

function klijentController($scope, $stateParams, $http, $rootScope) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/clients/" + $stateParams.id
  }).then(function (res) {
    console.log(res.data);
    $scope.klijent = res.data;
  });
}

function dodajProizvodController($scope, $ngConfirm, $http, $state, $rootScope) {

  // $scope.deo_proizvoda = [
  //   {
  //     naziv: "",
  //     skraceni_naziv: "",
  //     povrsina: ""
  //   }
  // ];

  $scope.naslovnaSlika = "";
  $scope.slikeProizvoda = [];

  $scope.$watch('naslovnaSlika', function (res) {
    if (res != '') {
      $scope.image = res;
    } else {
      $scope.image = '';
    }
    $scope.naslovna = res;
    if ($scope.put && res != '') {
      $http({
        method: "POST",
        data: res,
        url: $rootScope.url + "api/new-main-item-image/" + $scope.id
      }).then(function () {})
    }
  });

  $scope.getCategories = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/category"
    }).then(function (res) {
      $scope.categories = res.data.categories;
    })
  }
  $scope.getCategories();

  $scope.images = [];
  $scope.$watch('slikeProizvoda', function (res) {
    if (res.length != 0) {
      $scope.images.push(res);
    }
    if ($scope.put) {
      $('.loading').css('display', 'block');

      $http({
        method: "POST",
        url: $rootScope.url + "api/add-gallery-image/" + $scope.id,
        data: res
      }).then(function (response) {
        $('.loading').css('display', 'none');
      })
    }
  })


  // $scope.$watch('post', function(res) {
  //   console.log(res);
  //   if (res) {
  //     $('.addImageDiv input').attr('multiple', "on");
  //   } else {
  //     $('.addImageDiv input').attr('multiple', "off");
  //   }
  // })

  $scope.proizvod = {
    naziv_proizvoda: "",
    cena: "",
    zapremina: "",
    krojenje_vreme: "",
    tapaciranje_vreme: "",
    sivenje_vreme: "",
    priprema_vreme: "",
    parts: "",
    jedinica_mere: "KOM",
    broj_delova: 1,
    image: $scope.naslovna,
    images: $scope.images,
    sajt_prikaz: 1,
    webshop_prikaz: 1,
    akcija: 0,
    category_id: ""
  };

  $scope.appendNum = 1;
  $scope.deo_proizvoda = [];
  $scope.addPartInput = function () {
    $scope.deo_proizvoda.push({
      "naziv": "",
      "skraceni_naziv": "",
      "povrsina": ""
    });
  };

  $scope.getItems = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/items"
    }).then(function (res) {
      $scope.items = res.data;
    })
  };
  $scope.getItems();

  $scope.post = true;
  $scope.put = false;
  $scope.edit = function (id) {
    $scope.images = [];
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/items/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      console.log(res.data)

      var sajt = "";
      var webshop = "";
      var akcija = "";

      if (res.data.sajt_prikaz == 1) {
        sajt = true;
      } else {
        sajt = false;
      }
      ;

      if (res.data.webshop_prikaz == 1) {
        webshop = true;
      } else {
        webshop = false;
      }
      ;

      if (res.data.akcija == 1) {
        akcija = true;
      } else {
        akcija = false;
      }
      ;

      $scope.deo_proizvoda = [];
      for (var i = 0; i < res.data.parts.length; i++) {
        $scope.deo_proizvoda.push({
          id: res.data.parts[i].id,
          naziv: res.data.parts[i].naziv,
          skraceni_naziv: res.data.parts[i].skraceni_naziv,
          povrsina: res.data.parts[i].povrsina
        });
      }
      $scope.proizvod = {
        id: res.data.id,
        naziv_proizvoda: res.data.naziv_proizvoda,
        sifra_proizvoda: res.data.sifra_proizvoda,
        cena: res.data.cena,
        zapremina: res.data.zapremina,
        image: res.data.image,
        images: res.data.images,
        broj_delova: res.data.broj_delova,
        krojenje_vreme: res.data.krojenje_vreme,
        tapaciranje_vreme: res.data.tapaciranje_vreme,
        sivenje_vreme: res.data.sivenje_vreme,
        priprema_vreme: res.data.priprema_vreme,
        parts: $scope.deo_proizvoda,
        jedinica_mere: "KOM",
        sajt_prikaz: sajt,
        webshop_prikaz: webshop,
        akcija: akcija,
        category_id: res.data.category_id
      };

    })
  }

  $scope.deleteImage = function (index, id) {
    $http({
      method: "DELETE",
      url: $rootScope.url + "api/delete-gallery-image/" + id
    }).then(function () {
      $scope.proizvod.images.splice(index, 1);
    })
  }

  $scope.dodajProizvod = function (valid) {
    if (valid) {
      $scope.proizvod.image = $scope.naslovna;
      $scope.proizvod.parts = $scope.deo_proizvoda;
      if ($scope.post) {
        $http({
          method: "POST",
          data: $scope.proizvod,
          url: $rootScope.url + "api/items"
        }).then(function () {
          $state.reload();
        })
      } else {
        $http({
          method: "PUT",
          data: $scope.proizvod,
          url: $rootScope.url + "api/items/" + $scope.id
        }).then(function () {
          $state.reload();
        })
      }
    }
  }

  $scope.cancel = function () {
    $state.reload();
  }

  $scope.izbrisiDeo = function (index, deo) {
    if (deo.id == undefined) {
      $scope.deo_proizvoda.splice(index, 1);
    } else {
      $http({
        method: "DELETE",
        url: $rootScope.url + "api/part/" + deo.id
      }).then(function () {
        $scope.deo_proizvoda.splice(index, 1);
      })
    }
  };

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje proizvoda',
      contentUrl: '/views/admin/partials/prompt.html',
      buttons: {
        sayMyName: {
          text: 'Potvrdi',
          disabled: true,
          btnClass: 'btn-green',
          action: function (scope) {
            if (scope.password == 'staff') {
              $http({
                method: "DELETE",
                url: $rootScope.url + "api/items/" + id
              }).then(function () {
                $state.reload();
              })
            } else {
              $ngConfirm('Å ifra koju ste uneli nije validna.');
            }
          }
        },
        Odustani: function () {
        }
      },
      onScopeReady: function (scope) {
        var self = this;
        scope.textChange = function () {
          if (scope.password)
            self.buttons.sayMyName.setDisabled(false);
          else
            self.buttons.sayMyName.setDisabled(true);
        }
      }
    })
  }
}

function proizvodiListaController($scope, $rootScope, $http, $state) {
  $scope.getItems = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/items"
    }).then(function (res) {
      $scope.items = res.data;
    })
  };
  $scope.getItems();

  $scope.statistic = function (id) {
    $state.go('app.proizvod-statistika', {id: id})
  }
}

function proizvodStatistikaController($scope, $rootScope, $http, $stateParams) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/item-statistics/" + $stateParams.id
  }).then(function (res) {
    console.log(res.data);
    $scope.statistika = res.data;
    for (var i = 0; i < res.data.time_data.length; i++) {

      // Krojenje ostvareno vreme
      var logHoursK = Math.floor($scope.statistika.time_data[i].krojenje_total / 3600);
      var logMinutesK = Math.floor($scope.statistika.time_data[i].krojenje_total % 3600 / 60);
      var logSecondsK = Math.floor($scope.statistika.time_data[i].krojenje_total % 3600 % 60);
      $scope.statistika.time_data[i].logHkrojenje = logHoursK;
      $scope.statistika.time_data[i].logMkrojenje = logMinutesK;
      $scope.statistika.time_data[i].logSkrojenje = logSecondsK;

      // Sivenje ostvareno vreme
      var logHoursS = Math.floor($scope.statistika.time_data[i].sivenje_total / 3600);
      var logMinutesS = Math.floor($scope.statistika.time_data[i].sivenje_total % 3600 / 60);
      var logSecondsS = Math.floor($scope.statistika.time_data[i].sivenje_total % 3600 % 60);
      $scope.statistika.time_data[i].logHsivenje = logHoursS;
      $scope.statistika.time_data[i].logMsivenje = logMinutesS;
      $scope.statistika.time_data[i].logSsivenje = logSecondsS;

      // Tapaciranje ostvareno vreme
      var logHoursT = Math.floor($scope.statistika.time_data[i].tapaciranje_total / 3600);
      var logMinutesT = Math.floor($scope.statistika.time_data[i].tapaciranje_total % 3600 / 60);
      var logSecondsT = Math.floor($scope.statistika.time_data[i].tapaciranje_total % 3600 % 60);
      $scope.statistika.time_data[i].logHtapaciranje = logHoursT;
      $scope.statistika.time_data[i].logMtapaciranje = logMinutesT;
      $scope.statistika.time_data[i].logStapaciranje = logSecondsT;

      // Priprema ostvareno vreme
      var logHoursP = Math.floor($scope.statistika.time_data[i].priprema_total / 3600);
      var logMinutesP = Math.floor($scope.statistika.time_data[i].priprema_total % 3600 / 60);
      var logSecondsP = Math.floor($scope.statistika.time_data[i].priprema_total % 3600 % 60);
      $scope.statistika.time_data[i].logHpriprema = logHoursP;
      $scope.statistika.time_data[i].logMpriprema = logMinutesP;
      $scope.statistika.time_data[i].logSpriprema = logSecondsP;


      var hoursK = Math.floor($scope.statistika.time_data[i].krojenje_vreme / 60);
      var minutesK = $scope.statistika.time_data[i].krojenje_vreme % 60;

      $scope.statistika.time_data[i].norHoursK = hoursK;
      $scope.statistika.time_data[i].norMinutesK = minutesK;


      var hoursS = Math.floor($scope.statistika.time_data[i].sivenje_vreme / 60);
      var minutesS = $scope.statistika.time_data[i].sivenje_vreme % 60;

      $scope.statistika.time_data[i].norHoursS = hoursS;
      $scope.statistika.time_data[i].norMinutesS = minutesS;

      var hoursT = Math.floor($scope.statistika.time_data[i].tapaciranje_vreme / 60);
      var minutesT = $scope.statistika.time_data[i].tapaciranje_vreme % 60;

      $scope.statistika.time_data[i].norHoursT = hoursT;
      $scope.statistika.time_data[i].norMinutesT = minutesT;

      var hoursP = Math.floor($scope.statistika.time_data[i].priprema_vreme / 60);
      var minutesP = $scope.statistika.time_data[i].priprema_vreme % 60;

      $scope.statistika.time_data[i].norHoursP = hoursP;
      $scope.statistika.time_data[i].norMinutesP = minutesP;
    }

  })
}

function nalogController($scope, $http, $rootScope, $state) {

  $scope.getItems = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/items"
    }).then(function (res) {
      $scope.proizvodi = res.data;
    })
  };
  $scope.getItems();

  $scope.getMaterials = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/materials"
    }).then(function (res) {
      $scope.materials = res.data;
    })
  }
  $scope.getMaterials();

  $scope.getClients = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/clients"
    }).then(function (res) {
      $scope.kupci = res.data;
    })
  }
  $scope.getClients();

  $scope.nalog = {
    item_id: "",
    client_id: "",
    kolicina: 1,
    napomena_javna: "",
    adresa_isporuke: "",
    mesto_isporuke: "",
    napomena_privatna: "",
    ukrasni_step: "",
    parts: ""
  };

  $scope.deo = [{
    part_naziv: "",
    part_id: "",
    material: ""
  }];

  $scope.$watch('nalog.brojKomada', function (res) {
    if (res <= 1) {
      $scope.nalog.brojKomada = 1;
    }
  })

  setTimeout(function () {
    $('#kup').each(function () {
      var s = $(this);
      s.data().select2.on('focus', function (e) {
        s.select2('open');
      });
    });

    $('#pro').each(function () {
      var s = $(this);
      s.data().select2.on('focus', function (e) {
        s.select2('open');
      });
    });
  }, 500)

  $scope.adresaIsporuke = undefined;
  $scope.$watch('adresaIsporuke', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.nalog.mesto_isporuke = response.mesto;
      $scope.nalog.adresa_isporuke = response.adresa;
    }
  });

  // $scope.kupci = {
  //     tags: true
  // };

  // $scope.proizvodi = {
  //   tags: true
  // };

  $scope.$watch('nalog.kupac', function (res) {

    // var izKupac = $('#izabraniKupac').val();
    // var kupacNoQuote = izKupac.replace(/\"/g, '\\"');

    // var kupacId = $('#kupci option[value="' + kupacNoQuote + '"]').data('id');
    if (res != undefined) {
      $scope.nalog.client_id = res;
      $http({
        method: "GET",
        url: $rootScope.url + "api/clients/" + res
      }).then(function (res) {
        console.log(res.data);
        $scope.mestoIsporuke = res.data;
        if ($scope.mestoIsporuke.mesto_isporuke.length == 0) {
          $scope.mestoIsporuke.mesto_isporuke = [];
          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: $scope.mestoIsporuke.adresa,
            mesto: $scope.mestoIsporuke.mesto
          })
          $scope.nalog.mesto_isporuke = $scope.mestoIsporuke.mfesto;
          $scope.nalog.adresa_isporuke = $scope.mestoIsporuke.adresa;
        } else {
          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: $scope.mestoIsporuke.adresa,
            mesto: $scope.mestoIsporuke.mesto
          })
        }
      })
    }
  });


  $scope.$watch('nalog.proizvod', function (res) {
    // var izProizvod = $('#izabraniProizvod').val();
    // var proizvodNoQuote = izProizvod.replace(/\"/g, '\\"');
    // var proizvod = $('#proizvodi option[value="' + proizvodNoQuote + '"]').data('id');
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.delovi = true;
      $scope.parts = response.parts;
      $scope.proizvodId = response.id;
      $scope.deo = [];
      for (var i = 0; i < $scope.parts.length; i++) {
        $scope.deo.push({
          part_naziv: $scope.parts[i].naziv,
          part_id: $scope.parts[i].id,
          material: ""
        });
      }
    } else {
      $scope.delovi = false;
    }

    $scope.materijali = {
      tags: true
    };

    // for (var ii = 0; ii < $scope.deo.length; ii++) {
    //   $scope.$watch('deo[' + ii + '].material', function (res) {
    //
    //   })
    // }

    // if (res != undefined) {
    //   $scope.delovi = true;
    //   var response = JSON.parse(res);
    //   $scope.parts = response.parts;
    //   $scope.proizvodId = response.id;
    //   $scope.deo = [];
    //   for (var i = 0; i < $scope.parts.length; i++) {
    //     $scope.deo.push({
    //       part_naziv: $scope.parts[i].naziv,
    //       part_id: $scope.parts[i].id,
    //       material_id: ""
    //     });
    //   }
    // } else {
    //   $scope.delovi = false;
    // }
  });

  setTimeout(function () {
    // $('#pro').each(function () {
    //   var s = $(this);
    //   s.data().select2.on('focus', function (e) {
    //     s.select2('open');
    //   });
    // });
  }, 500)

  $scope.dodajNalog = function (valid) {
    $('.loading').css('display', 'block');
    if (valid) {
      $scope.nalog.item_id = $scope.proizvodId;
      $scope.nalog.parts = $scope.deo;
      // for (var i = 0; i < $scope.nalog.parts.length; i++) {
      //   if ($scope.parts[i].material_id != "") {
      //     var parse = JSON.parse($scope.nalog.parts[i].material_id);
      //     $scope.nalog.parts[i].material_id = parse.id;
      //     $scope.nalog.parts[i].material = parse.naziv_materijala;
      //   } else {
      //     $scope.nalog.parts[i].material = "";
      //   }
      // }

      $http({
        method: "POST",
        url: $rootScope.url + "api/orders",
        data: $scope.nalog
      }).then(function () {
        $('.loading').css('display', 'none');
        $state.reload();
      })
    }
  }
}

function nalogUpdateController($scope, $http, $stateParams, $state, $rootScope) {
  $scope.getOrder = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/orders/" + $stateParams.id
    }).then(function (res) {
      $scope.nalog = {
        item_id: res.data[0].item_id,
        proizvod: res.data[0].item.naziv_proizvoda,
        client: res.data[0].client.ime,
        client_id: res.data[0].client_id,
        kolicina: res.data[0].kolicina,
        napomena_javna: res.data[0].napomena_javna,
        obracunati_rabat: res.data[0].obracunati_rabat,
        adresa_isporuke: res.data[0].adresa_isporuke,
        mesto_isporuke: res.data[0].mesto_isporuke,
        ukrasni_step: res.data[0].ukrasni_step,
        napomena_privatna: res.data[0].napomena_privatna,
        parts: ""
      };

      $scope.mestoIsporuke = [];
      $scope.mestoIsporuke.push({
        adresa: $scope.nalog.adresa_isporuke,
        mesto: $scope.nalog.mesto_isporuke
      })


      $http({
        method: "GET",
        url: $rootScope.url + "api/clients/" + $scope.nalog.client_id
      }).then(function (res) {
        $scope.mestoIsporuke = res.data;
        if ($scope.mestoIsporuke.mesto_isporuke.length == 0) {
          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: res.data.adresa,
            mesto: res.data.mesto
          })
          // $scope.mestoIsporuke.mesto_isporuke.push({
          //   adresa: $scope.mestoIsporuke.adresa,
          //   mesto: $scope.mestoIsporuke.mesto
          // })
          // $scope.nalog.mesto_isporuke = $scope.mestoIsporuke.mesto_isporuke[0].mesto;
          // $scope.nalog.adresa_isporuke = $scope.mestoIsporuke.mesto_isporuke[0].adresa;
        } else {

          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: res.data.adresa,
            mesto: res.data.mesto
          })
        }

      })


      $scope.nalog.parts = [];
      $scope.deo = [];
      for (var i = 0; i < res.data[0].parts.length; i++) {
        $scope.deo.push({
          part_naziv: res.data[0].parts[i].naziv,
          part_id: res.data[0].parts[i].pivot.part_id,
          material: res.data[0].parts[i].pivot.material
        });
      }
      $scope.getMaterials();
      $scope.nalog.parts.push($scope.deo);
    })
  };
  $scope.getOrder();


  $scope.getMaterials = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/materials"
    }).then(function (res) {
      $scope.materials = res.data;
    })
  }
  $scope.getMaterials();

  $scope.materijali = {
    tags: true
  };

  $scope.cancel = function () {
    $state.go('home');
  }

  $scope.kupci = {
    tags: true
  };


  $scope.$watch('nalog.brojKomada', function (res) {
    if (res <= 1) {
      $scope.nalog.brojKomada = 1;
    }
  })

  $scope.adresaIsporuke = undefined;
  $scope.$watch('adresaIsporuke', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.nalog.mesto_isporuke = response.mesto;
      $scope.nalog.adresa_isporuke = response.adresa;
    }
  });


  $scope.azurirajNalog = function () {
    $scope.nalog.parts = $scope.deo;

    // for (var i = 0; i < $scope.nalog.parts.length; i++) {
    //   if (typeof $scope.nalog.parts[i].material_id != 'number') {
    //     var parse = JSON.parse($scope.nalog.parts[i].material_id);
    //     $scope.nalog.parts[i].material_id = parse.id;
    //     $scope.nalog.parts[i].material = parse.naziv_materijala;
    //   }
    // }

    console.log($scope.nalog)
    $http({
      method: "PUT",
      url: $rootScope.url + "api/orders/" + $stateParams.id,
      data: $scope.nalog
    }).then(function () {
      $state.go('home');
    })
  }
}

function deoProizvodaController($scope) {
  $('#deoProizvoda').editableSelect();
}

function materijalController($scope, $rootScope, $http, $state, $ngConfirm) {

  $scope.getMaterials = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/materials"
    }).then(function (res) {
      $scope.materials = res.data;
    })
  }
  $scope.getMaterials();

  $scope.materijali = {
    tags: true
  };

  $scope.kupci = {
    tags: true
  };

  $scope.post = true;
  $scope.put = false;

  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/materials/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      $scope.materijal = {
        naziv_materijala: res.data.naziv_materijala,
        jedinica_mere: res.data.jedinica_mere,
        cena_materijala: res.data.cena_materijala
      }
    })
  }

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje materijala',
      content: 'Da li ste sigurni da želite obrisati ovaj materijal?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/materials/" + id
            }).then(function () {
              $state.reload();
            })
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.cancel = function () {
    $state.reload();
  }

  $scope.dodajMaterijal = function () {
    if ($scope.post) {
      $http({
        method: "POST",
        url: $rootScope.url + "api/materials",
        data: $scope.materijal
      }).then(function () {
        $state.reload();
      })
    } else {
      $http({
        method: "PUT",
        url: $rootScope.url + "api/materials/" + $scope.id,
        data: $scope.materijal
      }).then(function () {
        $state.reload();
      })
    }
  }
}

function korisniciController($scope, $http, $rootScope, $state, $ngConfirm) {

  $scope.sektor = [
    {sector: "Nerasporedjen", sector_id: 1},
    {sector: "Administracija", sector_id: 2},
    {sector: "Krojenje", sector_id: 3},
    {sector: "Šivenje", sector_id: 4},
    {sector: "Tapaciranje", sector_id: 5},
    {sector: "Priprema", sector_id: 6},
  ]

  $scope.korisnik = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    sector_id: "",
    role_id: 1,
    qr: "",
    sector_id: "",
    client_id: ""
  };

  $scope.showPass = function (qr, name, lastname) {
    var decoded = atob(qr);

    var auth = decoded;
    var index = auth.indexOf(" ");
    var pass = auth.slice(index + 1, auth.length).trim();

    $scope.pass = pass;
    // $scope.name = name;
    // $scope.surname = lastname;
    $('.popUpDiv2').css('display', 'flex');
  }

  $scope.getClients = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/clients"
    }).then(function (res) {
      $scope.clients = res.data;
    })
  }


  $scope.cancel = function () {
    $state.reload();
  };

  $scope.statistic = function (userId) {
    $state.go('app.statistika', {id: userId});
  }

  $scope.qrEncoded = "";
  $scope.qr = function (qrCode, name, surname) {
    $('.popUpDiv').css('display', 'flex');
    $scope.qrEncoded = qrCode;
    $scope.ime = name;
    $scope.prezime = surname;
  }

  $scope.closePop = function (popType) {
    $('.' + popType).css('display', 'none');
  }

  $scope.getUsers = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/users"
    }).then(function (res) {
      $scope.users = res.data;
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      }
    });
  }
  $scope.getUsers();

  $scope.post = true;
  $scope.put = false;
  var admin;

  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/users/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      console.log(res.data);
      if (res.data.administrator == 0) {
        admin = false;
      } else {
        admin = true;
      }
      var decoded = atob(res.data.qr);
      var auth = decoded;
      var index = auth.indexOf(" ");
      var pass = auth.slice(index + 1, auth.length).trim();

      $scope.pass = pass;
      $scope.korisnik = {
        first_name: res.data.first_name,
        last_name: res.data.last_name,
        email: res.data.email,
        administrator: admin,
        password: pass,
        qr: res.data.qr,
        sector_id: res.data.sector_id,
        client_id: res.data.client_id
      }
    })
  };

  $scope.dodajKorisnika = function () {

    if ($scope.post) {
      if ($scope.korisnik.sector_id == "") {
        $scope.korisnik.sector_id = 1;
      }
      $scope.korisnik.qr = btoa($scope.korisnik.email + " " + $scope.korisnik.password);
      $http({
        method: "POST",
        url: $rootScope.url + "api/users",
        data: $scope.korisnik
      }).then(function () {
        $state.reload();
      })
    } else {
      $scope.korisnik.qr = btoa($scope.korisnik.email + " " + $scope.korisnik.password);
      $http({
        method: "PUT",
        url: $rootScope.url + "api/users/" + $scope.id,
        data: $scope.korisnik
      }).then(function () {
        $state.reload();
      })
    }
  };

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje korisnika',
      content: 'Da li ste sigurni da želite obrisati korisnika?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/users/" + id
            }).then(function () {
              $state.reload();
            })
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }
}

function planerController($scope, $http, $rootScope, $state, $ngConfirm, $timeout) {
  $scope.activeCeo = false;
  $scope.activeDeo = true;

  // if (localStorage.getItem('view') == 'ceo') {
  //   $timeout(function () {
  //     $scope.activeCeo = true;
  //     $scope.activeDeo = false;
  //     $('.planerTable').css('height', 'auto');
  //     $('.planerDiv > div:has(table)').attr('class', 'col-md-12');
  //     $('.planerDiv .table-responsive').css('border', 'unset');
  //   }, 1000)
  // } else if (localStorage.getItem('view') == 'tri') {
  //   $timeout(function () {
  //     $scope.activeCeo = false;
  //     $scope.activeTri = true;
  //     $scope.activeDeo = false;
  //     localStorage.setItem('view', 'tri');
  //     $('.planerTable').css('height', '400px');
  //     $('.planerDiv > div:has(table)').attr('class', 'col-md-4');
  //     $('.planerDiv > .col-md-4').css('margin-bottom', '30px');
  //     $('.planerDiv .table-responsive').css('border', '1px solid #e7e7e7');
  //   }, 1000)
  // } else {
  //   $('.planerDiv .table-responsive').css('border', 'unset');
  // }

  $scope.up = function (pravacIndex, orders, order, rbr) {

    var kandidat = "";
    for (var i = 0; i < orders.length; i++) {
      if (orders[i].red_br_utovara < order.red_br_utovara) {
        if (kandidat != '') {
          if (orders[i].red_br_utovara > kandidat.red_br_utovara) {
            kandidat = orders[i];
          }
        } else {
          kandidat = orders[i];
        }
      }
    }
    ;

    if (kandidat != "") {
      $scope.redosled = {
        order1_id: order.id,
        order1_redbr: order.red_br_utovara,
        order2_id: kandidat.id,
        order2_redbr: kandidat.red_br_utovara
      };
      $http({
        method: "POST",
        url: $rootScope.url + "api/orders/switch",
        data: $scope.redosled
      }).then(function (res) {
        if (res.data.success) {
          for (var n = 0; n < orders.length; n++) {
            if ($scope.redosled.order1_id == orders[n].id) {
              orders[n].red_br_utovara = $scope.redosled.order2_redbr;
            }
            if ($scope.redosled.order2_id == orders[n].id) {
              orders[n].red_br_utovara = $scope.redosled.order1_redbr;
            }
          }
        }
      })
    }
  };

  $scope.down = function (pravacIndex, orders, order, index) {

    var kandidat = "";
    for (var i = 0; i < orders.length; i++) {
      if (orders[i].red_br_utovara > order.red_br_utovara) {
        if (kandidat != '') {
          if (orders[i].red_br_utovara < kandidat.red_br_utovara) {
            kandidat = orders[i];
          }
        } else {
          kandidat = orders[i];
        }
      }
    }
    ;

    if (kandidat != "") {
      $scope.redosled = {
        order1_id: order.id,
        order1_redbr: order.red_br_utovara,
        order2_id: kandidat.id,
        order2_redbr: kandidat.red_br_utovara
      };
      $http({
        method: "POST",
        url: $rootScope.url + "api/orders/switch",
        data: $scope.redosled
      }).then(function (res) {
        if (res.data.success) {
          for (var n = 0; n < orders.length; n++) {
            if ($scope.redosled.order1_id == orders[n].id) {
              orders[n].red_br_utovara = $scope.redosled.order2_redbr;
            }
            if ($scope.redosled.order2_id == orders[n].id) {
              orders[n].red_br_utovara = $scope.redosled.order1_redbr;
            }
          }
        }
      })
    }
  };


  $scope.switchView = function (value) {
    if (value == 'ceo') {
      $scope.activeCeo = true;
      $scope.activeTri = false;
      $scope.activeDeo = false;
      localStorage.setItem('view', 'ceo');
      $('.planerTable').css('height', 'auto');
      $('.planerDiv > div:has(table)').attr('class', 'col-md-12');
      $('.planerDiv .table-responsive').css('border', 'unset');

    } else if (value == 'tri') {
      $scope.activeCeo = false;
      $scope.activeTri = true;
      $scope.activeDeo = false;
      localStorage.setItem('view', 'tri');
      $('.planerTable').css('height', '400px');
      $('.planerDiv > div:has(table)').attr('class', 'col-md-4');
      $('.planerDiv > .col-md-4').css('margin-bottom', '30px');
      $('.planerDiv .table-responsive').css('border', '1px solid #e7e7e7');
    } else {
      $scope.activeCeo = false;
      $scope.activeTri = false;
      $scope.activeDeo = true;
      localStorage.setItem('view', 'deo');
      $('.planerTable').css('height', '400px');
      $('.planerDiv > div:has(table)').attr('class', 'col-md-6');
      $('.planerDiv .table-responsive').css('border', 'unset');
    }
  };

  $scope.openContext = function (order, orderId, orderDatum) {
    $scope.orderIdContext = orderId;
    $scope.orderDatumContext = orderDatum;
    $scope.nerOrder = order;

    var div = $(".contextMenuOrder");
    div.css({
      display: "block",
      position: "absolute",
      top: event.pageY,
      left: event.pageX
    });
  };
  $('body').on('click', function () {
    $('.contextMenuOrder').css('display', 'none');
  });

  $scope.prebaciUshipment = function (shipId, nalog) {
    $scope.prebacivanje = {
      order: nalog,
      new_red_br_utovara: ""
    };
    // $scope.prebacivanje.orders.push(order.id);
    $http({
      method: "POST",
      data: $scope.prebacivanje,
      url: $rootScope.url + "api/orders/shipment/" + shipId
    }).then(function () {
      $state.reload();
    })
  };

  $scope.archive = function (id) {
    $ngConfirm({
      title: 'Arhiviranje pravca',
      content: 'Da li ste sigurni da želite arhiviranje ovog pravca?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "GET",
              url: $rootScope.url + "api/shipment/arhiviraj/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              } else if (res.data.error == 'token_expired') {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  };

  $scope.getShipments = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/shipments"
    }).then(function (res) {
      $scope.pravac = res.data;

      function sum(num1, num2) {
        return num1 + num2;
      }

      for (var i = 0; i < $scope.pravac.length; i++) {
        $scope.pravac[i].spremnoZaArhiviranje = true;
        $scope.pravac[i].zapremina = [];
        for (var n = 0; n < $scope.pravac[i].orders.length; n++) {
          if ($scope.pravac[i].orders[n].invoice_id == null) {
            $scope.pravac[i].spremnoZaArhiviranje = false;
          }
          $scope.pravac[i].zapremina.push($scope.pravac[i].orders[n].item.zapremina);
          $scope.pravac[i].orders[n].faktura = false;
          $scope.pravac[i].orders[n].created_at = new Date($scope.pravac[i].orders[n].created_at);
        }

        if ($scope.pravac[i].zapremina.length != 0) {
          var ukupnaZapremina = $scope.pravac[i].zapremina.reduce(sum)
          $scope.pravac[i].ukupnaZapremina = ukupnaZapremina;
        }

        var date = new Date($scope.pravac[i].datum_slanja);
        $scope.dayNo = date.getDay();
        if ($scope.dayNo == 1) {
          $scope.dan = "Ponedeljak";
        } else if ($scope.dayNo == 2) {
          $scope.dan = "Utorak";
        } else if ($scope.dayNo == 3) {
          $scope.dan = "Sreda";
        } else if ($scope.dayNo == 4) {
          $scope.dan = "Četvrtak";
        } else if ($scope.dayNo == 5) {
          $scope.dan = "Petak";
        } else if ($scope.dayNo == 6) {
          $scope.dan = "Subota";
        } else if ($scope.dayNo == 0) {
          $scope.dan = "Nedelja";
        }
        $scope.pravac[i].dan = $scope.dan;
      }
      setTimeout(function () {
        for (var b = 0; b < $scope.pravac.length; b++) {
          var tabs = $('#' + $scope.pravac[b].id);
          $("tbody.t_sortable").sortable({
            connectWith: ".t_sortable",
            items: "> tr:not(:first)",
            appendTo: tabs,
            helper: "clone",
            start: function () {
              $('.deloviTabela').hide();
            },
            zIndex: 999990
          }).disableSelection();

          $(tabs).droppable({
            accept: ".t_sortable tr",
            hoverClass: "ui-state-hover",
            tolerance: "touch"
          });

          $(tabs).on('drop', function (event, ui) {

            $('.loading').css('display', 'block');

            event.preventDefault();
            event.stopPropagation();

            $scope.newShipmentId = $(this).attr("id");
            $scope.newPosition;

            var currentIdx;
            setTimeout(function () {
              currentIdx = ui.draggable.index();
              // console.log(currentIdx);
              $scope.newPosition = currentIdx;
              // if (ui.draggable.prev().attr('value') == undefined && ui.draggable.next().attr('value') != undefined) {
              //   var parsed = JSON.parse(ui.draggable.next().attr('value'));
              //   if (currentIdx == parsed.red_br_utovara) {
              //     $scope.newPosition = parsed.red_br_utovara;
              //   }
              // } else if (ui.draggable.prev().attr('value') != undefined && ui.draggable.next().attr('value') != undefined) {
              //   var parsedPrev = JSON.parse(ui.draggable.prev().attr('value'));
              //   var parsedNext = JSON.parse(ui.draggable.next().attr('value'));
              //   prevPos = parsedPrev.red_br_utovara;
              //   nextPos = parsedNext.red_br_utovara;
              //
              //   if (currentIdx < nextPos) {
              //     $scope.newPosition = prevPos;
              //   } else if (currentIdx < prevPos) {
              //     $scope.newPosition = nextPos - 1;
              //   } else if (prevPos < currentIdx) {
              //     $scope.newPosition = prevPos + 1;
              //   }
              // } else if (ui.draggable.next().attr('value') == undefined && ui.draggable.prev().attr('value') != undefined) {
              //   var parsed = JSON.parse(ui.draggable.prev().attr('value'));
              //   if (currentIdx > parsed.red_br_utovara) {
              //     $scope.newPosition = parsed.red_br_utovara + 1;
              //   } else if (currentIdx == parsed.red_br_utovara) {
              //     $scope.newPosition = parsed.red_br_utovara;
              //   }
              // }
              //
              // if (ui.draggable.prev().attr('value') == undefined && ui.draggable.next().attr('value') == undefined) {
              //   $scope.newPosition = 1;
              // }

              var nalog = JSON.parse(ui.draggable.attr("value"));

              $scope.prebacivanje = {
                order: nalog,
                new_red_br_utovara: $scope.newPosition
              };

              $http({
                method: "POST",
                data: $scope.prebacivanje,
                url: $rootScope.url + "api/orders/shipment/" + $scope.newShipmentId
              }).then(function () {
                $scope.getShipments();
                $scope.fixRedBr = function () {
                  $http({
                    method: "GET",
                    url: $rootScope.url + "api/shipmentsfixorders"
                  }).then(function () {
                  })
                }
                $scope.fixRedBr();
              })
            }, 400);
            return false;
          })
        }
        console.log($scope.pravac)
      }, 1000);

      if (localStorage.getItem('view') == 'ceo') {
        $timeout(function () {
          $scope.activeCeo = true;
          $scope.activeTri = false;
          $scope.activeDeo = false;
          $('.planerTable').css('height', 'auto');
          $('.planerDiv > div:has(table)').attr('class', 'col-md-12');
          $('.planerDiv .table-responsive').css('border', 'unset');
        }, 100)
      } else if (localStorage.getItem('view') == 'tri') {
        $timeout(function () {
          $scope.activeCeo = false;
          $scope.activeTri = true;
          $scope.activeDeo = false;
          localStorage.setItem('view', 'tri');
          $('.planerTable').css('height', '400px');
          $('.planerDiv > div:has(table)').attr('class', 'col-md-4');
          $('.planerDiv > .col-md-4').css('margin-bottom', '30px');
          $('.planerDiv .table-responsive').css('border', '1px solid #e7e7e7');
        }, 100)
      } else {
        $timeout(function () {
          $('.planerDiv .table-responsive').css('border', 'unset');
          $scope.activeCeo = false;
          $scope.activeTri = false;
          $scope.activeDeo = true;
        }, 100)
      }
      $('.loading').css('display', 'none');

    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };
  $scope.getShipments();

  var selected = false;
  $scope.selektujeSve = function (index) {
    if (!selected) {
      for (var n = 0; n < $scope.pravac[index].orders.length; n++) {
        $scope.pravac[index].orders[n].faktura = true;
        $scope.pravac[index].allSelected = true;
      }
      selected = true;
    } else {
      for (var n = 0; n < $scope.pravac[index].orders.length; n++) {
        $scope.pravac[index].orders[n].faktura = false;
        $scope.pravac[index].allSelected = false;
      }
      selected = false;
    }
  }

  $scope.zaFakturisanje = [];
  $scope.fakturisanje = function () {
    for (var i = 0; i < $scope.pravac.length; i++) {
      for (var n = 0; n < $scope.pravac[i].orders.length; n++) {
        if ($scope.pravac[i].orders[n].faktura) {
          $scope.zaFakturisanje.push($scope.pravac[i].orders[n]);
          $scope.clientId = $scope.zaFakturisanje[0].client_id;
        }
      }
    }
    var exist;
    for (var m = 0; m < $scope.zaFakturisanje.length; m++) {
      if ($scope.zaFakturisanje[m].client_id != $scope.clientId) {
        exist = true;
      } else {
        exist = false
      }
    }
    if (exist) {
      $ngConfirm({
        title: 'Fakturisanje',
        content: 'Fakturisanje naloga je moguće samo istim klijentom.',
        type: 'blue',
        typeAnimated: true,
        buttons: {
          Ne: {
            text: "U redu",
            btnClass: 'btn-blue',
            action: function () {
              // $('.planerCheck').attr('checked', false);
              // $scope.zaFakturisanje = [];
              // for (var i = 0; i < $scope.pravac.length; i++) {
              //   for (var n = 0; n < $scope.pravac[i].orders.length; n++) {
              //    $scope.pravac[i].orders[n].faktura = false;
              //   }
              // }
              $state.reload();
            }
          }
        }
      })
    } else {
      if ($scope.zaFakturisanje.length == 0) {
        $ngConfirm({
          title: 'Fakturisanje',
          content: 'Niste izabrali naloge za fakturisanje',
          type: 'blue',
          typeAnimated: true,
          buttons: {
            Ne: {
              text: "U redu",
              btnClass: 'btn-blue',
              action: function () {
                $state.reload();
              }
            }
          }
        })
      } else {
        $state.go('app.faktura', {orders: $scope.zaFakturisanje});
      }
    }
  };

  $scope.getOrders = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/orders"
    }).then(function (res) {
      $scope.nalozi = res.data;
      for (var i = 0; i < $scope.nalozi.length; i++) {
        $scope.nalozi[i].created_at = new Date($scope.nalozi[i].created_at)
      }
    })
  };
  $scope.getOrders();

  $scope.printNalog = function (id, aktivnost) {
    // if sklanja mogucnost stampanja ako je nalog vec jednom odstampan
    // if (aktivnost == null) {
    $http({
      method: "GET",
      url: $rootScope.url + "api/orders/print/aktivnosti/" + id
    }).then(function (res) {
      if (res.data.success) {
        $state.go('print-nalog', {id: id});
      }
    })
    // }
  };

  $scope.printNalogPrip = function (id, aktivnost) {
    // if sklanja mogucnost stampanja ako je nalog vec jednom odstampan
    // if (aktivnost == null) {
    $http({
      method: "GET",
      url: $rootScope.url + "api/orders/print/priprema/" + id
    }).then(function (res) {
      if (res.data.success) {
        $state.go('print-nalog-prip', {id: id});
      }
    })
    // }
  };


  $scope.printShipment = function (id) {
    $state.go('print-shipment', {id: id});
  };

  $scope.printAdresnaNal = function (id) {
    $state.go('adresna-nalepnica', {id: id});
  }

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje naloga',
      content: 'Da li ste sigurni da želite obrisati ovaj nalog?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/orders/" + id
            }).then(function () {
              $state.reload();
            })
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.deleteShipNalog = function (nalog) {
    $scope.prebacivanje = {
      order: nalog,
      new_red_br_utovara: ""
    };
    $ngConfirm({
      title: 'Brisanje naloga iz pravca',
      content: 'Da li ste sigurni da želite da obrišete nalog iz ovog pravca?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "POST",
              url: $rootScope.url + "api/orders/shipment/0",
              data: $scope.prebacivanje
            }).then(function () {
              $state.reload();
            })
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })

  }

  $scope.finish = function (pravac, nalog) {
    $scope.orderToShipment = {
      order: nalog,
      new_red_br_utovara: null
    };
    // $scope.orderToShipment.orders.push(order_id);
    $http({
      method: "POST",
      url: $rootScope.url + "api/orders/shipment/" + pravac,
      data: $scope.orderToShipment
    }).then(function () {
      $state.reload();
    })
  }
}

function vozilaController($scope, $http, $rootScope, $state, $ngConfirm) {

  $scope.vozilo = {
    registracija: "",
    tip_vozila: "",
    nosivost: "",
    zapremina: ""
  }

  $scope.post = true;
  $scope.put = false;

  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/vehicles/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      $scope.vozilo = {
        registracija: res.data[0].registracija,
        tip_vozila: res.data[0].tip_vozila,
        nosivost: res.data[0].nosivost,
        zapremina: res.data[0].zapremina
      }
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };

  $scope.cancel = function () {
    $state.reload();
  };

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje vozila',
      content: 'Da li ste sigurni da želite obrisati vozilo?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/vehicles/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.getVechicles = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/vehicles"
    }).then(function (res) {
      $scope.vehicles = res.data;
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }
  $scope.getVechicles();

  $scope.dodajVozilo = function () {
    if ($scope.post) {
      $http({
        method: "POST",
        data: $scope.vozilo,
        url: $rootScope.url + "api/vehicles"
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    } else {
      $http({
        method: "PUT",
        data: $scope.vozilo,
        url: $rootScope.url + "api/vehicles/" + $scope.id
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    }
  }
}

function vozaciController($scope, $http, $rootScope, $state, $ngConfirm) {

  $scope.vozac = {
    ime_vozaca: "",
    licna_karta: ""
  };

  $scope.post = true;
  $scope.put = false;

  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/driver/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      $scope.vozac = {
        ime_vozaca: res.data.ime_vozaca,
        licna_karta: res.data.licna_karta
      }
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };

  $scope.cancel = function () {
    $state.reload();
  };

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje vozaÄa',
      content: 'Da li ste sigurni da želite obrisati vozaÄa?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/driver/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.getDrivers = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/driver"
    }).then(function (res) {
      $scope.vozaci = res.data;
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }
  $scope.getDrivers();

  $scope.dodajVozaca = function () {
    if ($scope.post) {
      $http({
        method: "POST",
        data: $scope.vozac,
        url: $rootScope.url + "api/driver"
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    } else {
      $http({
        method: "PUT",
        data: $scope.vozac,
        url: $rootScope.url + "api/driver/" + $scope.id
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    }
  }
}

function dodajPravacController($scope, $http, $rootScope, $state, $ngConfirm) {
  $scope.post = true;
  $scope.put = false;
  $http({
    method: "GET",
    url: $rootScope.url + "api/vehicles"
  }).then(function (res) {
    $scope.vozila = res.data;
  }).catch(function (res) {
    if (res.data.error == "token_not_provided") {
      $state.go('login')
    } else if (res.data.error == 'token_expired') {
      $state.go('login')
    }
  });

  $scope.pravac = {
    pravac: "",
    vehicle_id: "",
    datum_slanja: ""
  }

  $scope.getShipments = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/shipments"
    }).then(function (res) {
      $scope.shipments = res.data;
      for (var i = 0; i < $scope.shipments.length; i++) {
        var date = new Date($scope.shipments[i].datum_slanja);
        $scope.dayNo = date.getDay();
        if ($scope.dayNo == 1) {
          $scope.dan = "Ponedeljak";
        } else if ($scope.dayNo == 2) {
          $scope.dan = "Utorak";
        } else if ($scope.dayNo == 3) {
          $scope.dan = "Sreda";
        } else if ($scope.dayNo == 4) {
          $scope.dan = "Četvrtak";
        } else if ($scope.dayNo == 5) {
          $scope.dan = "Petak";
        } else if ($scope.dayNo == 6) {
          $scope.dan = "Subota";
        } else if ($scope.dayNo == 0) {
          $scope.dan = "Nedelja";
        }
        $scope.shipments[i].dan = $scope.dan;
      }
      console.log($scope.shipments);
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };
  $scope.getShipments();

  // $scope.svi = true;
  // $scope.arhivirano = false;
  // $scope.prikazPravaca = function(prikaz) {
  //   if (prikaz == 'svi') {
  //     $scope.svi = true;
  //     $scope.arhivirano = false;
  //   } else {
  //     $scope.svi = false;
  //     $scope.arhivirano = true;
  //   }
  // }

  $scope.post = true;
  $scope.put = false;
  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/shipments/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      if (res.data.vehicle == null) {
        var vehicleId = "";
        var tip = "";
      } else {
        vehicleId = res.data.vehicle.id;
        tip = res.data.vehicle.tip_vozila;
      }
      $scope.pravac = {
        pravac: res.data.pravac,
        vehicle_id: vehicleId,
        tip: tip,
        datum_slanja: new Date(res.data.datum_slanja)
      }
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje pravca',
      content: 'Da li ste sigurni da želite obrisati ovaj pravac?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/shipments/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              } else if (res.data.error == 'token_expired') {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  };
  $scope.archive = function (id) {
    $ngConfirm({
      title: 'Arhiviranje pravca',
      content: 'Da li ste sigurni da želite arhiviranje ovog pravca?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "GET",
              url: $rootScope.url + "api/shipment/arhiviraj/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              } else if (res.data.error == 'token_expired') {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  };

  $scope.dodajPravac = function () {
    var date = $scope.pravac.datum_slanja.getFullYear() + "-" + ($scope.pravac.datum_slanja.getMonth() + 1) + "-" + $scope.pravac.datum_slanja.getDate();
    $scope.pravac.datum_slanja = date;
    if ($scope.post) {
      $http({
        method: "POST",
        url: $rootScope.url + "api/shipments",
        data: $scope.pravac
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    } else {
      $http({
        method: "PUT",
        url: $rootScope.url + "api/shipments/" + $scope.id,
        data: $scope.pravac
      }).then(function () {
        $state.reload();
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    }
  }
}

function printNalogController($scope, $http, $stateParams, $rootScope) {

  $scope.bc = {
    format: 'CODE128',
    lineColor: '#000000',
    width: 2,
    height: 80,
    displayValue: true,
    fontOptions: '',
    font: 'monospace',
    textAlign: 'center',
    textPosition: 'bottom',
    textMargin: 2,
    fontSize: 17,
    background: '#ffffff',
    margin: 0,
    marginTop: undefined,
    marginBottom: undefined,
    marginLeft: undefined,
    marginRight: undefined,
    valid: function (valid) {
    }
  }

  $http({
    method: "GET",
    url: $rootScope.url + "api/orders/" + $stateParams.id
  }).then(function (res) {
    $scope.nalog = res.data[0];
    $scope.date = new Date();

    console.log(res.data[0]);
    $scope.nalog.aktivnosti_datum = new Date(res.data[0].aktivnosti_datum);
    $scope.nalog.created_at = new Date(res.data[0].created_at);
    setTimeout(function () {
      window.print();
    }, 700)
  }).catch(function (res) {
    if (res.data.error == "token_not_provided") {
      $state.go('login')
    } else if (res.data.error == 'token_expired') {
      $state.go('login')
    }
  });
}

function printNalogPripController($scope, $http, $stateParams, $rootScope) {
  $scope.bc = {
    format: 'CODE128',
    lineColor: '#000000',
    width: 2,
    height: 80,
    displayValue: true,
    fontOptions: '',
    font: 'monospace',
    textAlign: 'center',
    textPosition: 'bottom',
    textMargin: 2,
    fontSize: 17,
    background: '#ffffff',
    margin: 0,
    marginTop: undefined,
    marginBottom: undefined,
    marginLeft: undefined,
    marginRight: undefined,
    valid: function (valid) {
    }
  }

  $http({
    method: "GET",
    url: $rootScope.url + "api/orders/" + $stateParams.id
  }).then(function (res) {
    $scope.nalog = res.data[0];
    $scope.date = new Date();
    $scope.nalog.aktivnosti_datum = new Date(res.data[0].aktivnosti_datum);
    $scope.nalog.created_at = new Date(res.data[0].created_at);
    setTimeout(function () {
      window.print();
    }, 700)
  }).catch(function (res) {
    if (res.data.error == "token_not_provided") {
      $state.go('login')
    } else if (res.data.error == 'token_expired') {
      $state.go('login')
    }
  });
}

function printShipmentController($scope, $http, $stateParams, $rootScope) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/shipments/" + $stateParams.id
  }).then(function (res) {
    $scope.shipment = res.data;
    console.log(res.data);
    var date = new Date($scope.shipment.datum_slanja);
    $scope.dayNo = date.getDay();
    if ($scope.dayNo == 1) {
      $scope.dan = "Ponedeljak";
    } else if ($scope.dayNo == 2) {
      $scope.dan = "Utorak";
    } else if ($scope.dayNo == 3) {
      $scope.dan = "Sreda";
    } else if ($scope.dayNo == 4) {
      $scope.dan = "Četvrtak";
    } else if ($scope.dayNo == 5) {
      $scope.dan = "Petak";
    } else if ($scope.dayNo == 6) {
      $scope.dan = "Subota";
    } else if ($scope.dayNo == 0) {
      $scope.dan = "Nedelja";
    }

    $scope.shipment.dan = $scope.dan;
    setTimeout(function () {
      window.print();
    }, 700);
  }).catch(function (res) {
    if (res.data.error == "token_not_provided") {
      $state.go('login')
    } else if (res.data.error == 'token_expired') {
      $state.go('login')
    }
  });
}

function printAdresnaNalController($scope, $http, $rootScope, $stateParams) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/orders/" + $stateParams.id
  }).then(function (res) {
    $scope.nalog = res.data[0];
    setTimeout(function () {
      window.print();
    }, 500)
  }).catch(function (res) {
    if (res.data.error == "token_not_provided") {
      $state.go('login')
    } else if (res.data.error == 'token_expired') {
      $state.go('login')
    }
  });
}

function fakturaController($scope, $http, $state, $rootScope, $stateParams, $ngConfirm) {

  window.scrollTo(0, 0);

  $scope.cancel = function () {
    $state.go('home');
  };

  $scope.printZaVozaca = function () {
    //
    // $('#zaVozaca').css('display', 'block');
    // $('#regularnaFaktura').css('display', 'none');
    $state.go('app.printFaktura', {faktura: $scope.faktura})
    // setTimeout(function () {
    //   window.print();
    // }, 500)
  }

  $scope.nalozi = $stateParams.orders;

  $http({
    method: "GET",
    url: $rootScope.url + "api/clients/" + $scope.nalozi[0].client.id
  }).then(function (res) {
    $scope.mestoIsporuke = res.data;
    if ($scope.mestoIsporuke.mesto_isporuke.length == 0) {
      $scope.mestoIsporuke.mesto_isporuke = [];
      $scope.mestoIsporuke.mesto_isporuke.push({
        adresa: $scope.mestoIsporuke.adresa,
        mesto: $scope.mestoIsporuke.mesto
      })
    } else {
      $scope.mestoIsporuke.mesto_isporuke.push({
        adresa: $scope.mestoIsporuke.adresa,
        mesto: $scope.mestoIsporuke.mesto
      })
    }
  })


  $scope.izvoznaFak = {
    koleta: "",
    paritet: "",
    bruto: "",
    neto: "",
    reg_br: "",
    vozac: "",
    br_licne: ""
  };

  $scope.getVechicles = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/vehicles"
    }).then(function (res) {
      $scope.vehicles = res.data;
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }
  $scope.getVechicles();

  $scope.getDrivers = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/driver"
    }).then(function (res) {
      $scope.vozaci = res.data;
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }
  $scope.getDrivers();

  $scope.$watch('vozilo', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.izvoznaFak.reg_br = response.registracija;
    }
  })

  $scope.$watch('vozac', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.izvoznaFak.vozac = response.ime_vozaca;
      $scope.izvoznaFak.br_licne = response.licna_karta;
    }
  })

  console.log($scope.nalozi);
  $scope.date = new Date($scope.nalozi[0].created_at);

  $scope.adresaIsporuke = undefined;
  $scope.$watch('adresaIsporuke', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);

      $scope.faktura.kupac = $scope.kupac;
      $scope.faktura.mesto_isporuke = response.mesto;
      $scope.faktura.adresa_isporuke = response.adresa;
    } else {
      $scope.faktura.mesto_isporuke = $scope.nalozi[0].mesto_isporuke;
      $scope.faktura.adresa_isporuke = $scope.nalozi[0].adresa_isporuke;
    }
  });

  $scope.kupac = {
    ime: $scope.nalozi[0].client.ime,
    adresa: $scope.nalozi[0].client.adresa,
    mesto: $scope.nalozi[0].client.mesto,
    pib: $scope.nalozi[0].client.pib
  };

  $scope.faktura = {
    valuta_placanja: 0,
    faktura_broj: "",
    otpremnica_broj: "",
    datum_izdavanja: new Date(),
    datum_prometa: "",
    // datum_valute: "",
    napomena_oslobodjenje: "Nema",
    napomena: "",
    stopa_pdv: "20",
    avans: 0,
    srednji_kurs: 118,
    tip_fakture: "",
    orders: "",
    kupac: $scope.kupac,
  };


  $scope.izvozna = false;
  $scope.$watch('faktura.tip_fakture', function (res) {
    if (res == true) {
      $scope.izvozna = true;
      $scope.faktura.napomena_oslobodjenje = "Oslobođeno plaćanja PDV-a u skladu sa čl. 24 stav 1 tačka 2 Zakona o PDV-u"
      $scope.faktura.napomena = "Izvoznik proizvoda obuhvaćenih ovom ispravom izjavljuje da su, osim ako je to drugačije izričito navedeno, ovi proizvodi Srpskog preferencionog porekla."
      $scope.getInvoiceNumber = function () {
        $http({
          method: "GET",
          url: $rootScope.url + "api/invoice/brojfakture/1"
        }).then(function (res) {
          $scope.faktura.faktura_broj = res.data.faktura_broj;
          $scope.faktura.otpremnica_broj = res.data.faktura_broj;
          $scope.year = new Date();
        }).catch(function (res) {
          if (res.data.error == "token_not_provided") {
            $state.go('login')
          } else if (res.data.error == 'token_expired') {
            $state.go('login')
          }
        });
      };
      $scope.getInvoiceNumber();
      $scope.faktura.izvoznaFaktura = $scope.izvoznaFak;
    } else {
      $scope.faktura.napomena_oslobodjenje = "";
      $scope.faktura.napomena = "";
      $scope.izvozna = false;
      $scope.faktura.izvoznaFaktura = $scope.izvoznaFak;
    }
  });

  $scope.getInvoiceNumber = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/invoice/brojfakture/0"
    }).then(function (res) {
      $scope.faktura.faktura_broj = res.data.faktura_broj;
      $scope.year = new Date();
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };

  $scope.getInvoiceNumber();

  $scope.$watch('faktura.srednji_kurs', function (res) {
    if (res != "0") {
      $scope.eur = true;
      $scope.faktura.srednji_kurs = res;
    } else {
      $scope.eur = false;
      $scope.faktura.srednji_kurs = 1;
    }
  });

  $scope.$watch('faktura.avans', function (res) {
    $scope.avansnaUplata = res;
  })

  $scope.zaUplatu = [];
  $scope.$watch('faktura.stopa_pdv', function (res) {
    if (res == "0") {
      $scope.eur = false;
      $scope.pdv = false;
      $scope.getInvoiceNumber = function () {
        $http({
          method: "GET",
          url: $rootScope.url + "api/invoice/brojfakture/2"
        }).then(function (res) {
          $scope.faktura.faktura_broj = res.data.faktura_broj;
          $scope.faktura.otpremnica_broj = res.data.faktura_broj;
          $scope.year = new Date();
        }).catch(function (res) {
          if (res.data.error == "token_not_provided") {
            $state.go('login')
          } else if (res.data.error == 'token_expired') {
            $state.go('login')
          }
        });
      };
      $scope.getInvoiceNumber();
    } else if (res == "10") {
      $scope.eur = false;
      $scope.pdv = true;
      $scope.getInvoiceNumber = function () {
        $http({
          method: "GET",
          url: $rootScope.url + "api/invoice/brojfakture/2"
        }).then(function (res) {
          $scope.faktura.faktura_broj = res.data.faktura_broj;
          $scope.faktura.otpremnica_broj = res.data.faktura_broj;
          $scope.year = new Date();
        }).catch(function (res) {
          if (res.data.error == "token_not_provided") {
            $state.go('login')
          } else if (res.data.error == 'token_expired') {
            $state.go('login')
          }
        });
      };
      $scope.getInvoiceNumber();
    } else {
      $scope.eur = true;
      $scope.pdv = true;
      $scope.getInvoiceNumber = function () {
        $http({
          method: "GET",
          url: $rootScope.url + "api/invoice/brojfakture/0"
        }).then(function (res) {
          $scope.faktura.faktura_broj = res.data.faktura_broj;
          $scope.faktura.otpremnica_broj = res.data.faktura_broj;
          $scope.year = new Date();
        }).catch(function (res) {
          if (res.data.error == "token_not_provided") {
            $state.go('login')
          } else if (res.data.error == 'token_expired') {
            $state.go('login')
          }
        });
      };
      $scope.getInvoiceNumber();
    }

    $scope.zaUplatu = [];
    $scope.poreskeO = [];
    $scope.ukupnoRabat = [];
    $scope.ukupnoPDV = [];
    $scope.ukupnoCena = [];

    for (var i = 0; i < $stateParams.orders.length; i++) {
      $scope.nalozi[i].item.cena = parseInt($stateParams.orders[i].item.cena);
      $scope.ukupnoCena.push($stateParams.orders[i].item.cena * $stateParams.orders[i].kolicina);
    }
    $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);

    $scope.calculateRabatPoskupljenje = function () {
      $scope.zaUplatu = [];
      $scope.poreskeO = [];
      $scope.ukupnoRabat = [];
      $scope.ukupnoPDV = [];
      $scope.ukupnoCena = [];
      for (var n = 0; n < $stateParams.orders.length; n++) {
        var cena = $stateParams.orders[n].item.cena * $stateParams.orders[n].kolicina;
        $scope.poreskaOsnovica = cena * (1 + $scope.nalozi[n].poskupljenje / 100) * (1 - $scope.nalozi[n].rabat / 100)

        $scope.nalozi[n].poreska_osnovica = $scope.poreskaOsnovica;

        $scope.iznosPdv = $scope.nalozi[n].poreska_osnovica * res / 100;
        $scope.nalozi[n].cenaSaPdv = $scope.iznosPdv;

        $scope.zaUplatu.push($scope.nalozi[n].cenaSaPdv + $scope.nalozi[n].poreska_osnovica);
        $scope.poreskeO.push($scope.nalozi[n].poreska_osnovica);
        $scope.ukupnoPDV.push($scope.nalozi[n].cenaSaPdv);

        if ($scope.eur) {
          $scope.ukupnoRabat.push(((cena * $scope.faktura.srednji_kurs) * (1 + $scope.nalozi[n].poskupljenje / 100)) * ($scope.nalozi[n].rabat / 100));
          $scope.ukupnoCena.push((parseInt($stateParams.orders[n].item.cena) * $scope.faktura.srednji_kurs) * (1 + $scope.nalozi[n].poskupljenje / 100) * parseInt($stateParams.orders[n].kolicina));
        } else {
          $scope.ukupnoRabat.push(((cena) * (1 + $scope.nalozi[n].poskupljenje / 100)) * ($scope.nalozi[n].rabat / 100));
          $scope.ukupnoCena.push(parseInt($stateParams.orders[n].item.cena * (1 + $scope.nalozi[n].poskupljenje / 100)) * parseInt($stateParams.orders[n].kolicina));
        }

        $scope.ukupnoZaUplatu = $scope.zaUplatu.reduce(sum);
        $scope.poreskeOsnovice = $scope.poreskeO.reduce(sum);
        $scope.ukupnoRabatCena = $scope.ukupnoRabat.reduce(sum);
        $scope.ukupanIznosPDV = $scope.ukupnoPDV.reduce(sum);
        $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);

      }
    };

    for (var n = 0; n < $stateParams.orders.length; n++) {
      $scope.iznosPdv = $stateParams.orders[n].poreska_osnovica * res / 100;
      $scope.nalozi[n].cenaSaPdv = $scope.iznosPdv;
      $scope.zaUplatu.push($scope.nalozi[n].cenaSaPdv + $scope.nalozi[n].poreska_osnovica);
      $scope.nalozi[n].rabat = 0;
      $scope.nalozi[n].poskupljenje = 0;
    }
    $scope.ukupnoZaUplatu = $scope.zaUplatu.reduce(sum);
    $scope.calculateRabatPoskupljenje();
  })

  $scope.poreskeO = [];

  function sum(num1, num2) {
    return num1 + num2;
  }

  $.datepicker.regional['sr'] = {
    closeText: 'Затвори',
    prevText: '&#x3c;',
    nextText: '&#x3e;',
    currentText: 'Данас',
    monthNames: ['Јануар', 'Фебруар', 'Март', 'Април', 'Мај', 'Јун',
      'Јул', 'Август', 'Септембар', 'Октобар', 'Новембар', 'Децембар'],
    monthNamesShort: ['Јан', 'Феб', 'Мар', 'Апр', 'Мај', 'Јун',
      'Јул', 'Авг', 'Сеп', 'Окт', 'Нов', 'Дец'],
    dayNames: ['Недеља', 'Понедељак', 'Уторак', 'Среда', 'Четвртак', 'Петак', 'Субота'],
    dayNamesShort: ['Нед', 'Пон', 'Уто', 'Сре', 'Чет', 'Пет', 'Суб'],
    dayNamesMin: ['Не', 'По', 'Ут', 'Ср', 'Че', 'Пе', 'Су'],
    weekHeader: 'Сед',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['sr']);
  $('#datumIzdavanja').datepicker();
  $('#datumPrometa').datepicker();

  $scope.datumIzdavanja = new Date();
  $scope.datumPrometa = new Date();

  $scope.dodajFakturu = function () {
    $scope.faktura.ukupnaPoreska = $scope.poreskeOsnovice;
    $scope.faktura.ukupnoZaUplatu = $scope.ukupnoZaUplatu;

    $scope.faktura.datum_izdavanja = $scope.datumIzdavanja;
    $scope.faktura.datum_prometa = $scope.datumPrometa;

    $scope.datumIzdavanja = new Date($scope.datumIzdavanja);
    $scope.datumPrometa = new Date($scope.datumPrometa);
    if ($scope.faktura.datum_izdavanja != null) {
      $scope.faktura.datum_izdavanja = $scope.datumIzdavanja.getFullYear() + "-" + ($scope.datumIzdavanja.getMonth() + 1) + "-" + $scope.datumIzdavanja.getDate();
    }
    if ($scope.faktura.datum_prometa != null) {
      $scope.faktura.datum_prometa = $scope.datumPrometa.getFullYear() + "-" + ($scope.datumPrometa.getMonth() + 1) + "-" + $scope.datumPrometa.getDate();
    }

    if (!$scope.eur) {
      $scope.faktura.sum = $scope.ukupnoZaUplatu;
    } else {
      $scope.faktura.sum = $scope.ukupnoZaUplatu * $scope.faktura.srednji_kurs;
    }
    $scope.faktura.orders = $scope.nalozi;


    if ($scope.izvoznaFak.reg_br != "") {
      $scope.faktura.reg_broj = $scope.izvoznaFak.reg_br;
    } else {
      $scope.faktura.reg_broj = "";
    }
    if ($scope.izvoznaFak.vozac != "") {
      $scope.faktura.vozac = $scope.izvoznaFak.vozac;
    } else {
      $scope.faktura.vozac = "";
    }
    if ($scope.izvoznaFak.br_licne != "") {
      $scope.faktura.broj_licne = $scope.izvoznaFak.br_licne;
    } else {
      $scope.faktura.broj_licne = "";
    }

    if ($scope.izvoznaFak.bruto != "") {
      $scope.faktura.bruto = $scope.izvoznaFak.bruto;
    } else {
      $scope.faktura.bruto = null;
    }
    if ($scope.izvoznaFak.paritet != "") {
      $scope.faktura.paritet = $scope.izvoznaFak.paritet;
    } else {
      $scope.faktura.paritet = null;
    }
    if ($scope.izvoznaFak.neto != "") {
      $scope.faktura.neto = $scope.izvoznaFak.neto;
    } else {
      $scope.faktura.neto = null;
    }
    if ($scope.izvoznaFak.koleta != "") {
      $scope.faktura.koleta = $scope.izvoznaFak.koleta;
    } else {
      $scope.faktura.koleta = null;
    }

    if (!$scope.izvozna) {
      if (!$scope.eur) {
        $scope.faktura.tip_fakture = 2;
      } else {
        $scope.faktura.tip_fakture = 0;
      }

      // $state.go('printFaktura', {faktura: $scope.faktura})

      $http({
        method: "POST",
        url: $rootScope.url + "api/invoices",
        data: $scope.faktura
      }).then(function (res) {
        $state.go('printFaktura', {faktura: $scope.faktura})
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        } else if (res.data.error == 'Nije uspelo snimanje fakture u bazu podataka') {
          $ngConfirm({
            title: 'Greška prilikom pravljenja fakture.',
            content: '<b>Nije uspelo snimanje fakture.</b>',
            type: 'red',
            typeAnimated: true,
            buttons: {
              uredu: {
                text: 'U redu',
                btnClass: 'btn-red',
                action: function (scope, button) {
                }
              }
            }
          })
        }
      });
    } else {
      $scope.faktura.tip_fakture = 1;
      $http({
        method: "POST",
        url: $rootScope.url + "api/invoices",
        data: $scope.faktura
      }).then(function () {
        $state.go('izvoznaFaktura', {faktura: $scope.faktura});
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    }
  }

}

function printFakturaController($scope, $http, $stateParams) {
  window.scrollTo(0, 0);

  $scope.faktura = $stateParams.faktura;

  console.log($scope.faktura)

  function sum(num1, num2) {
    return num1 + num2;
  }

  $scope.faktura.datum_izdavanja = new Date($scope.faktura.datum_izdavanja);
  $scope.faktura.datum_prometa = new Date($scope.faktura.datum_prometa);

  $scope.dIzdavanja = new Date($scope.faktura.datum_izdavanja);

  var datumIzdavanja = $scope.faktura.datum_izdavanja;
  $scope.zadnjiDatumValute = datumIzdavanja.add($scope.faktura.valuta_placanja).days();

  setTimeout(function () {
    if ($scope.faktura.orders.length >= 8 && $scope.faktura.orders.length <= 25) {
      $('.fakturaTable').after("<div style=\"clear:both!important;\"/></div>\n" +
        "<div style=\"page-break-after:always\"></div> \n" +
        "<div style=\"clear:both!important;\"/> </div>" +
        "<div style='height: 80px;width: 100%;'></div>")
    } else {
      $('th').css('padding', '3px');
      $('td').css('padding', '3px');
      $(".fakturaTable tr").each(function (index) {
        index += 1;
        if (index % 25 == 0) {
          $(this).after("<div style=\"clear:both!important;\"/></div>\n" +
            "<div style=\"page-break-after:always\"></div> \n" +
            "<div style=\"clear:both!important;\"/> </div>" +
            "<div style='height: 80px;width: 100%;'></div>");
        }
      });
    }
    window.print();
  }, 1500);

  $scope.year = new Date();

  if ($scope.faktura.stopa_pdv == "0") {
    $scope.eur = false;
    $scope.pdv = false;
  } else if ($scope.faktura.stopa_pdv == "10") {
    $scope.eur = false;
    $scope.pdv = true;
  } else {
    $scope.eur = true;
    $scope.pdv = true;
  }

  $scope.printZaVozaca = function () {
    $scope.vozac = true;
    setTimeout(function () {
      window.print();
    }, 1500);
    for (var i = 0; i < $scope.faktura.orders.length; i++) {
      $scope.faktura.avans = 0;
      $scope.faktura.orders[i].item.cena = 0;
      $scope.faktura.orders[i].cenaSaPdv = 0;
      $scope.faktura.orders[i].poreska_osnovica = 0;
      $scope.faktura.stopa_pdv = 0;
    }
    $scope.ukupnoZaUplatu = 0;
    $scope.poreskeOsnovice = 0;
    $scope.ukupnoRabatCena = 0;
    $scope.ukupnaCena = 0;
    $scope.ukupanIznosPDV = 0;
  }

  $scope.calculateRabatPoskupljenje = function () {
    $scope.zaUplatu = [];
    $scope.poreskeO = [];
    $scope.ukupnoRabat = [];
    $scope.ukupnoPDV = [];
    for (var n = 0; n < $scope.faktura.orders.length; n++) {
      var cena = $scope.faktura.orders[n].item.cena * $scope.faktura.orders[n].kolicina;
      $scope.poreskaOsnovica = cena * (1 + $scope.faktura.orders[n].poskupljenje / 100) * (1 - $scope.faktura.orders[n].rabat / 100)

      // $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;
      // $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
      // $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);

      $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;

      $scope.iznosPdv = $scope.faktura.orders[n].poreska_osnovica * $scope.faktura.stopa_pdv / 100;
      $scope.faktura.orders[n].cenaSaPdv = $scope.iznosPdv;

      $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
      $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);
      $scope.ukupnoPDV.push($scope.faktura.orders[n].cenaSaPdv);

      if ($scope.eur) {
        $scope.ukupnoRabat.push(((cena * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
        // var uc = $scope.ukupnoZaUplatu * 118;
      } else {
        $scope.ukupnoRabat.push(((cena) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
      }
    }

    $scope.ukupnoZaUplatu = $scope.zaUplatu.reduce(sum);
    $scope.poreskeOsnovice = $scope.poreskeO.reduce(sum);
    $scope.ukupnoRabatCena = $scope.ukupnoRabat.reduce(sum);
    $scope.ukupanIznosPDV = $scope.ukupnoPDV.reduce(sum);
  }
  $scope.calculateRabatPoskupljenje();

  $scope.ukupnoCena = [];
  for (var i = 0; i < $scope.faktura.orders.length; i++) {
    $scope.faktura.orders[i].created_at = new Date($scope.faktura.orders[i].created_at);
    if ($scope.eur) {
      $scope.ukupnoCena.push((parseInt($scope.faktura.orders[i].item.cena) * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[i].poskupljenje / 100) * parseInt($scope.faktura.orders[i].kolicina));
    } else {
      $scope.ukupnoCena.push(parseInt($stateParams.faktura.orders[i].item.cena * (1 + $scope.faktura.orders[i].poskupljenje / 100)) * parseInt($scope.faktura.orders[i].kolicina));
    }
  }

  $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);

  var uc;
  if ($scope.faktura.stopa_pdv == '10' || $scope.faktura.stopa_pdv == '0') {
    // $scope.printZaVozaca();
    uc = Math.floor($scope.ukupnoZaUplatu - $scope.faktura.avans) - $scope.faktura.avans;
    $scope.euro = true;
  } else {
    uc = Math.floor($scope.ukupnoZaUplatu * $scope.faktura.srednji_kurs) - $scope.faktura.avans;
    $scope.decimale = (($scope.ukupnoZaUplatu * $scope.faktura.srednji_kurs) - $scope.faktura.avans) * 100 % 100;
  }

  var a = ['', 'jedna', 'dve', 'tri', 'četiri', 'pet', 'šest', 'sedam', 'osam', 'devet', 'deset', 'jedanaest', 'dvanaest', 'trinaest', 'četrnaest', 'petnaest', 'šestnaest', 'sedamnaest', 'osamnaest', 'devetnaest'];
  var b = ['', '', 'dvadeset', 'trideset', 'četrdeset', 'pedeset', 'šestdeset', 'sedamdeset', 'osamdeset', 'devetdeset'];

  if ((uc = uc.toString()).length > 9) return 'overflow';
  n = ('000000000' + uc).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  if (!n) return;
  var str = '';
  str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + '' + a[n[1][1]]) + 'crore' : '';
  str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + '' + a[n[2][1]]) + 'sto' : '';
  str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + '' + a[n[3][1]]) + 'hiljada' : '';
  str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + '' + a[n[4][1]]) + 'stotina' : '';
  str += (n[5] != 0) ? ((str != '') ? '' : '') + (a[Number(n[5])] || b[n[5][0]] + '' + a[n[5][1]]) + '' : '';

  $scope.slovima = str;
  // setTimeout(function () {
  //   window.print();
  // }, 1500);
}

function faktureController($scope, $http, $state, $rootScope, $ngConfirm) {

  $scope.datePicker = {startDate: null, endDate: null};

  window.scrollTo(0, 0);

  $scope.datePickerOptions = {
    locale: {
      applyLabel: "Potvrdi",
      fromLabel: "Od",
      format: "YYYY-MM-DD", //will give you 2017-01-06
      //format: "D-MMM-YY", //will give you 6-Jan-17
      //format: "D-MMMM-YY", //will give you 6-January-17
      toLabel: "Do",
      cancelLabel: 'Odustani',
      customRangeLabel: 'Odaberi (Od - Do)'
    },
    ranges: {
      'Zadnjih 7 Dana': [moment().subtract(6, 'days'), moment()],
      'Zadnjih 30 Dana': [moment().subtract(29, 'days'), moment()],
      'Zadnjih 60 Dana': [moment().subtract(59, 'days'), moment()]
    }
  }

  $scope.getFakture = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/invoices"
    }).then(function (res) {
      $scope.fakture = res.data;
      for (var i = 0; i < $scope.fakture.length; i++) {
        $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
      }
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  }
  $scope.getFakture();


  $scope.$watchGroup(['datePicker', 'tipFakture.stopa_pdv'], function (res) {


    if (res[0].startDate != undefined && res[0].endDate != undefined) {
      var start = res[0].startDate._d;
      var end = res[0].endDate._d;

      var startDate = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
      var endDate = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate();

      $scope.faktureStartDate = startDate;
      $scope.faktureEndDate = endDate;

      // if ($scope.tipFakture.stopa_pdv == 0) {
      //   $scope.faktura_tip = 2;
      // } else if ($scope.tipFakture.stopa_pdv == 10) {
      //    $scope.faktura_tip = 1;
      // } else if ($scope.tipFakture.stopa_pdv == 20 || $scope.tipFakture.stopa_pdv == undefined) {
      //    $scope.faktura_tip = 0;
      // }

      if (res[1] == undefined) {
        res[1] = "";
      }

      $scope.filter = {
        dateFrom: startDate,
        dateTo: endDate,
        stopa_pdv: res[1]
      }

      $http({
        method: "POST",
        data: $scope.filter,
        url: $rootScope.url + "api/invoices/filtered"
      }).then(function (res) {
        $scope.fakture = res.data;
        console.log(res.data);
        for (var i = 0; i < $scope.fakture.length; i++) {
          $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
        }
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    } else {

      // $scope.getFakture();
    }
  });

  $scope.tipFakture = [
    {stopa_pdv: 0},
    {stopa_pdv: 10},
    {stopa_pdv: 20},
  ]

  $scope.removeDate = function () {
    $state.reload();
  }

  $scope.$watch('tipFakture.stopa_pdv', function (res) {

    if ($scope.faktureStartDate == undefined || $scope.faktureEndDate == undefined) {
      $scope.faktureStartDate = null;
      $scope.faktureEndDate = null;
    }

    if (res != '20' && res != undefined) {
      $scope.fakturaFilter = {
        tip_fakture: 2,
        stopa_pdv: res,
        dateFrom: $scope.faktureStartDate,
        dateTo: $scope.faktureEndDate,
      };
      $http({
        method: "POST",
        data: $scope.fakturaFilter,
        url: $rootScope.url + "api/invoices/filtered"
      }).then(function (res) {
        $scope.fakture = res.data;
        for (var i = 0; i < $scope.fakture.length; i++) {
          $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
        }
      })
    } else if (res == '20') {
      $scope.fakturaFilter = {
        tip_fakture: 0,
        stopa_pdv: res,
        dateFrom: $scope.faktureStartDate,
        dateTo: $scope.faktureEndDate,
      };
      $http({
        method: "POST",
        data: $scope.fakturaFilter,
        url: $rootScope.url + "api/invoices/filtered"
      }).then(function (res) {
        $scope.fakture = res.data;
        for (var i = 0; i < $scope.fakture.length; i++) {
          $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
        }
      })
    }
    if (res != undefined) {
      $scope.fakturaFilter = {
        tip_fakture: 0,
        stopa_pdv: res,
        dateFrom: $scope.faktureStartDate,
        dateTo: $scope.faktureEndDate,
      };
      $http({
        method: "POST",
        data: $scope.fakturaFilter,
        url: $rootScope.url + "api/invoices/filtered"
      }).then(function (res) {
        $scope.fakture = res.data;
        for (var i = 0; i < $scope.fakture.length; i++) {
          $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
        }
      })
    }
    if (res == "") {
      $scope.fakturaFilter = {
        tip_fakture: 0,
        stopa_pdv: null,
        dateFrom: $scope.faktureStartDate,
        dateTo: $scope.faktureEndDate,
      };
      $http({
        method: "POST",
        data: $scope.fakturaFilter,
        url: $rootScope.url + "api/invoices/filtered"
      }).then(function (res) {
        $scope.fakture = res.data;
        for (var i = 0; i < $scope.fakture.length; i++) {
          $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
        }
      })
    }
  })

  $scope.$watch('izvozna', function (res) {
    if (res != undefined) {
      if (res) {
        $scope.izvoznaChecked = true;
        $scope.fakturaFilter = {
          tip_fakture: 1,
          stopa_pdv: 0
        }
        $http({
          method: "POST",
          data: $scope.fakturaFilter,
          url: $rootScope.url + "api/invoices/filtered"
        }).then(function (res) {
          $scope.fakture = res.data;
          for (var i = 0; i < $scope.fakture.length; i++) {
            $scope.fakture[i].created_at = new Date($scope.fakture[i].created_at);
          }
        })
      } else {
        $scope.izvoznaChecked = false;
        $scope.getFakture();
      }
    }
  })

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje fakture',
      content: '<b>Prilikom brisanja fakture svi nalozi iz fakture prelaze u nerasporeÄ‘ene i nefakturisane naloge.</b>',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/invoices/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              } else if (res.data.error == 'token_expired') {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.print = function (id) {
    $state.go('printGotovaFaktura', {id: id})
  }
}

function fakturaDetaljnoController($scope, $http, $rootScope, $stateParams, $state) {
  $scope.zaUplatu = [];
  $scope.poreskeO = [];
  $scope.ukupnoCena = [];
  $scope.ukupnoRabat = [];

  $scope.print = function (id, vozac) {
    if (vozac != undefined) {
      $state.go('printGotovaFaktura', {id: id, vozac: vozac})
    } else {
      $state.go('printGotovaFaktura', {id: id, vozac: ""})
    }
  };

  window.scrollTo(0, 0);

  // setTimeout(function () {
  //   if ($scope.faktura.orders.length >= 8 && $scope.faktura.orders.length <= 25) {
  //     $('.fakturaTable').after("<div style=\"clear:both!important;\"/></div>\n" +
  //       "<div style=\"page-break-after:always\"></div> \n" +
  //       "<div style=\"clear:both!important;\"/> </div>" +
  //       "<div style='height: 80px;width: 100%;'></div>")
  //   } else {
  //     $('th').css('padding', '3px');
  //     $('td').css('padding', '3px');
  //     $(".fakturaTable tr").each(function (index) {
  //       index += 1;
  //       if (index % 25 == 0) {
  //         $(this).after("<div style=\"clear:both!important;\"/></div>\n" +
  //           "<div style=\"page-break-after:always\"></div> \n" +
  //           "<div style=\"clear:both!important;\"/> </div>" +
  //           "<div style='height: 80px;width: 100%;'></div>");
  //       }
  //     });
  //   }
  //   window.print();
  // }, 1500);

  $scope.year = new Date();

  $scope.getFaktura = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/invoices/" + $stateParams.id
    }).then(function (res) {
      $scope.faktura = res.data;

      function sum(num1, num2) {
        return num1 + num2;
      }

      $scope.faktura.datum_izdavanja = new Date($scope.faktura.datum_izdavanja);
      $scope.faktura.datum_prometa = new Date($scope.faktura.datum_prometa);

      $scope.dIzdavanja = new Date($scope.faktura.datum_izdavanja);

      var datumIzdavanja = $scope.faktura.datum_izdavanja;
      $scope.zadnjiDatumValute = datumIzdavanja.add($scope.faktura.valuta_placanja).days();

      $scope.year = new Date();

      console.log($scope.faktura);

      if ($scope.faktura.stopa_pdv == "0.00") {
        $scope.eur = false;
        $scope.pdv = false;
      } else if ($scope.faktura.stopa_pdv == "10.00") {
        $scope.eur = false;
        $scope.pdv = true;
      } else {
        $scope.eur = true;
        $scope.pdv = true;
      }

      $scope.calculateRabatPoskupljenje = function () {
        $scope.zaUplatu = [];
        $scope.poreskeO = [];
        $scope.ukupnoRabat = [];
        $scope.ukupnoPDV = [];
        for (var n = 0; n < $scope.faktura.orders.length; n++) {
          var cena = $scope.faktura.orders[n].item.cena * $scope.faktura.orders[n].kolicina;
          $scope.poreskaOsnovica = cena * (1 + $scope.faktura.orders[n].poskupljenje / 100) * (1 - $scope.faktura.orders[n].rabat / 100)

          // $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;
          // $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
          // $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);

          $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;

          $scope.iznosPdv = $scope.faktura.orders[n].poreska_osnovica * $scope.faktura.stopa_pdv / 100;
          $scope.faktura.orders[n].cenaSaPdv = $scope.iznosPdv;

          $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
          $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);
          $scope.ukupnoPDV.push($scope.faktura.orders[n].cenaSaPdv);

          if ($scope.eur) {
            $scope.ukupnoRabat.push(((cena * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
          } else {
            $scope.ukupnoRabat.push(((cena) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
          }
        }

        $scope.ukupnoZaUplatu = $scope.zaUplatu.reduce(sum);
        $scope.poreskeOsnovice = $scope.poreskeO.reduce(sum);
        $scope.ukupnoRabatCena = $scope.ukupnoRabat.reduce(sum);
        $scope.ukupanIznosPDV = $scope.ukupnoPDV.reduce(sum);
      }
      $scope.calculateRabatPoskupljenje();

      $scope.ukupnoCena = [];
      for (var i = 0; i < $scope.faktura.orders.length; i++) {
        $scope.faktura.orders[i].created_at = new Date($scope.faktura.orders[i].created_at);
        if ($scope.eur) {
          $scope.ukupnoCena.push((parseInt($scope.faktura.orders[i].item.cena) * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[i].poskupljenje / 100) * parseInt($scope.faktura.orders[i].kolicina));
        } else {
          $scope.ukupnoCena.push(parseInt($scope.faktura.orders[i].item.cena * (1 + $scope.faktura.orders[i].poskupljenje / 100)) * parseInt($scope.faktura.orders[i].kolicina));
        }
      }

      $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);

      // setTimeout(function () {
      //   window.print();
      // }, 1500);
    })
  };


  $scope.getFaktura();

}

function printGotovaFakturaController($scope, $http, $rootScope, $stateParams) {
  $scope.zaUplatu = [];
  $scope.poreskeO = [];
  $scope.ukupnoCena = [];
  $scope.ukupnoRabat = [];

  window.scrollTo(0, 0);

  $scope.year = new Date();

  $scope.getFaktura = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/invoices/" + $stateParams.id
    }).then(function (res) {
      $scope.faktura = res.data;

      console.log(res.data);

      function sum(num1, num2) {
        return num1 + num2;
      }

      $scope.faktura.datum_izdavanja = new Date($scope.faktura.datum_izdavanja);
      $scope.faktura.datum_prometa = new Date($scope.faktura.datum_prometa);

      $scope.dIzdavanja = new Date($scope.faktura.datum_izdavanja);

      var datumIzdavanja = $scope.faktura.datum_izdavanja;
      $scope.zadnjiDatumValute = datumIzdavanja.add($scope.faktura.valuta_placanja).days();

      if ($scope.faktura.tip_fakture == 1) {
        $('.regularna').css('display', 'none');
        $('.izvoznaFak').css('display', 'block');
      }

      if ($stateParams.vozac == 'zaVozaca') {
        $scope.vozac = true;

        for (var i = 0; i < $scope.faktura.orders.length; i++) {
          $scope.faktura.avans = 0;
          $scope.faktura.orders[i].cena_faktura = 0;
          $scope.faktura.orders[i].cenaSaPdv = 0;
          $scope.faktura.orders[i].rabat = 0;
          $scope.faktura.orders[i].poskupljenje = 0;
          $scope.faktura.orders[i].poreska_osnovica = 0;
          $scope.faktura.stopa_pdv = 0;
        }

        $scope.eur = true;

        $scope.ukupnoZaUplatu = 0;
        $scope.poreskeOsnovice = 0;
        $scope.ukupnoRabatCena = 0;
        $scope.ukupanIznosPDV = 0;
        $scope.ukupnaCena = 0;
        setTimeout(function () {
          if ($scope.faktura.orders.length >= 8 && $scope.faktura.orders.length <= 25) {
            $('.fakturaTable').after("<div style=\"clear:both!important;\"/></div>\n" +
              "<div style=\"page-break-after:always\"></div> \n" +
              "<div style=\"clear:both!important;\"/> </div>" +
              "<div style='height: 80px;width: 100%;'></div>")
          } else {
            $('th').css('padding', '3px');
            $('td').css('padding', '3px');
            $(".fakturaTable tr").each(function (index) {
              index += 1;
              if (index % 25 == 0) {
                $(this).after("<div style=\"clear:both!important;\"/></div>\n" +
                  "<div style=\"page-break-after:always\"></div> \n" +
                  "<div style=\"clear:both!important;\"/> </div>" +
                  "<div style='height: 80px;width: 100%;'></div>");
              }
            });
          }
          window.print();
        }, 1500);

      } else {
        setTimeout(function () {
          if ($scope.faktura.orders.length >= 8 && $scope.faktura.orders.length <= 25) {
            $('.fakturaTable').after("<div style=\"clear:both!important;\"/></div>\n" +
              "<div style=\"page-break-after:always\"></div> \n" +
              "<div style=\"clear:both!important;\"/> </div>" +
              "<div style='height: 80px;width: 100%;'></div>")
          } else {
            $('th').css('padding', '3px');
            $('td').css('padding', '3px');
            $(".fakturaTable tr").each(function (index) {
              index += 1;
              if (index % 25 == 0) {
                $(this).after("<div style=\"clear:both!important;\"/></div>\n" +
                  "<div style=\"page-break-after:always\"></div> \n" +
                  "<div style=\"clear:both!important;\"/> </div>" +
                  "<div style='height: 80px;width: 100%;'></div>");
              }
            });
          }
          window.print();
        }, 1500);

        $scope.year = new Date();

        if ($scope.faktura.stopa_pdv == "0.00") {
          $scope.eur = false;
          $scope.pdv = false;
        } else if ($scope.faktura.stopa_pdv == "10.00") {
          $scope.eur = false;
          $scope.pdv = true;
        } else {
          $scope.eur = true;
          $scope.pdv = true;
        }

        $scope.calculateRabatPoskupljenje = function () {
          $scope.zaUplatu = [];
          $scope.poreskeO = [];
          $scope.ukupnoRabat = [];
          $scope.ukupnoPDV = [];
          for (var n = 0; n < $scope.faktura.orders.length; n++) {
            var cena = $scope.faktura.orders[n].item.cena * $scope.faktura.orders[n].kolicina;
            $scope.poreskaOsnovica = cena * (1 + $scope.faktura.orders[n].poskupljenje / 100) * (1 - $scope.faktura.orders[n].rabat / 100);

            // $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;
            // $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
            // $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);

            $scope.faktura.orders[n].poreska_osnovica = $scope.poreskaOsnovica;

            $scope.iznosPdv = $scope.faktura.orders[n].poreska_osnovica * $scope.faktura.stopa_pdv / 100;
            $scope.faktura.orders[n].cenaSaPdv = $scope.iznosPdv;

            $scope.zaUplatu.push($scope.faktura.orders[n].cenaSaPdv + $scope.faktura.orders[n].poreska_osnovica);
            $scope.poreskeO.push($scope.faktura.orders[n].poreska_osnovica);
            $scope.ukupnoPDV.push($scope.faktura.orders[n].cenaSaPdv);

            if ($scope.eur) {
              $scope.ukupnoRabat.push(((cena * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
              // var uc = $scope.ukupnoZaUplatu * 118;
            } else {
              $scope.ukupnoRabat.push(((cena) * (1 + $scope.faktura.orders[n].poskupljenje / 100)) * ($scope.faktura.orders[n].rabat / 100));
            }
          }

          $scope.ukupnoZaUplatu = $scope.zaUplatu.reduce(sum);
          $scope.poreskeOsnovice = $scope.poreskeO.reduce(sum);
          $scope.ukupnoRabatCena = $scope.ukupnoRabat.reduce(sum);
          $scope.ukupanIznosPDV = $scope.ukupnoPDV.reduce(sum);
        }
        $scope.calculateRabatPoskupljenje();

        $scope.ukupnoCena = [];
        for (var i = 0; i < $scope.faktura.orders.length; i++) {
          $scope.faktura.orders[i].created_at = new Date($scope.faktura.orders[i].created_at);
          if ($scope.eur) {
            $scope.ukupnoCena.push((parseInt($scope.faktura.orders[i].item.cena) * $scope.faktura.srednji_kurs) * (1 + $scope.faktura.orders[i].poskupljenje / 100) * parseInt($scope.faktura.orders[i].kolicina));
          } else {
            $scope.ukupnoCena.push(parseInt($scope.faktura.orders[i].item.cena * (1 + $scope.faktura.orders[i].poskupljenje / 100)) * parseInt($scope.faktura.orders[i].kolicina));
          }
        }

        $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);
        var uc;
        if ($scope.faktura.stopa_pdv == '10.00' || $scope.faktura.stopa_pdv == '0.00') {
          uc = Math.floor($scope.ukupnoZaUplatu - $scope.faktura.avans) - $scope.faktura.avans;
          $scope.euro = true;
        } else if ($scope.faktura.tip_fakture == '1') {
          uc = parseInt($scope.faktura.sum);
        } else {
          uc = Math.floor($scope.ukupnoZaUplatu * $scope.faktura.srednji_kurs) - $scope.faktura.avans;
          $scope.decimale = (($scope.ukupnoZaUplatu * $scope.faktura.srednji_kurs) - $scope.faktura.avans) * 100 % 100;
        }

        var a = ['', 'jedna', 'dve', 'tri', 'četiri', 'pet', 'šest', 'sedam', 'osam', 'devet', 'deset', 'jedanaest', 'dvanaest', 'trinaest', 'četrnaest', 'petnaest', 'šestnaest', 'sedamnaest', 'osamnaest', 'devetnaest'];
        var b = ['', '', 'dvadeset', 'trideset', 'četrdeset', 'pedeset', 'šestdeset', 'sedamdeset', 'osamdeset', 'devetdeset'];

        if ((uc = uc.toString()).length > 9) return 'overflow';
        n = ('000000000' + uc).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + '' + a[n[1][1]]) + 'crore' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + '' + a[n[2][1]]) + 'sto' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + '' + a[n[3][1]]) + 'hiljada' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + '' + a[n[4][1]]) + 'stotina' : '';
        str += (n[5] != 0) ? ((str != '') ? '' : '') + (a[Number(n[5])] || b[n[5][0]] + '' + a[n[5][1]]) + '' : '';

        $scope.slovima = str;
      }

      // setTimeout(function () {
      //   window.print();
      // }, 1500);
    })
  };
  $scope.getFaktura();
}

function izvoznaFakturaController($scope, $stateParams) {
  $scope.faktura = $stateParams.faktura;
  $scope.year = new Date();

  window.scrollTo(0, 0);

  function sum(num1, num2) {
    return num1 + num2;
  }

  setTimeout(function () {
    if ($scope.faktura.orders.length >= 10 && $scope.faktura.orders.length <= 25) {
      $('.fakturaTable').after("<div style=\"clear:both!important;\"/></div>\n" +
        "<div style=\"page-break-after:always\"></div> \n" +
        "<div style=\"clear:both!important;\"/> </div>" +
        "<div style='height: 80px;width: 100%;'></div>")
    } else {
      $('th').css('padding', '3px');
      $('td').css('padding', '3px');
      $(".fakturaTable tr").each(function (index) {
        index += 1;
        if (index % 25 == 0) {
          $(this).after("<div style=\"clear:both!important;\"/></div>\n" +
            "<div style=\"page-break-after:always\"></div> \n" +
            "<div style=\"clear:both!important;\"/> </div>" +
            "<div style='height: 80px;width: 100%;'></div>");
        }
      });
    }
    window.print();
  }, 1500);

  $scope.ukupnoCena = [];
  for (var i = 0; i < $scope.faktura.orders.length; i++) {
    $scope.faktura.orders[i].created_at = new Date($scope.faktura.orders[i].created_at);
    $scope.faktura.datum_izdavanja = new Date($scope.faktura.datum_izdavanja);
    $scope.ukupnoCena.push(parseInt($scope.faktura.orders[i].item.cena) * parseInt($scope.faktura.orders[i].kolicina));
  }
  $scope.ukupnaCena = $scope.ukupnoCena.reduce(sum);

  var uc = $scope.ukupnaCena;

  var a = ['', 'jedna', 'dve', 'tri', 'četiri', 'pet', 'šest', 'sedam', 'osam', 'devet', 'deset', 'jedanaest', 'dvanaest', 'trinaest', 'četrnaest', 'petnaest', 'šestnaest', 'sedamnaest', 'osamnaest', 'devetnaest'];
  var b = ['', '', 'dvadeset', 'trideset', 'četrdeset', 'pedeset', 'šestdeset', 'sedamdeset', 'osamdeset', 'devetdeset'];

  if ((uc = uc.toString()).length > 9) return 'overflow';
  n = ('000000000' + uc).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
  if (!n) return;
  var str = '';
  str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + '' + a[n[1][1]]) + 'crore' : '';
  str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + '' + a[n[2][1]]) + 'sto' : '';
  str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + '' + a[n[3][1]]) + 'hiljada' : '';
  str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + '' + a[n[4][1]]) + 'stotina' : '';
  str += (n[5] != 0) ? ((str != '') ? '' : '') + (a[Number(n[5])] || b[n[5][0]] + '' + a[n[5][1]]) + '' : '';

  $scope.slovima = str;
}

function naloziController($scope, $http, $rootScope, $ngConfirm) {
  $scope.getOrders = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/orders"
    }).then(function (res) {
      $scope.nalozi = res.data;
      for (var i = 0; i < $scope.nalozi.length; i++) {
        $scope.nalozi[i].created_at = new Date($scope.nalozi[i].created_at)
      }
    }).catch(function (res) {
      if (res.data.error == "token_not_provided") {
        $state.go('login')
      } else if (res.data.error == 'token_expired') {
        $state.go('login')
      }
    });
  };
  $scope.getOrders();

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje naloga',
      content: 'Da li ste sigurni da želite obrisati ovaj nalog?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/orders/" + id
            }).then(function () {
              $state.reload();
            }).catch(function (res) {
              if (res.data.error == "token_not_provided") {
                $state.go('login')
              } else if (res.data.error == 'token_expired') {
                $state.go('login')
              }
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }
}

function loginController($scope, $http, $rootScope, $state, AuthenticationService) {
  // initController()
  //
  // function initController() {
  //   // reset login status
  //   AuthenticationService.Logout();
  // };

  //
  // $scope.login = function () {
  //   AuthenticationService.Login($scope.data.email, $scope.data.password, function (result) {
  //     if (result === true) {
  //       $state.go('home');
  //     } else {
  //       // vm.error = 'Username or password is incorrect';
  //       console.log(result)
  //     }
  //   });
  // };
  //
  $scope.data = {
    email: "",
    password: ""
  }
  $scope.login = function () {
    $http({
      method: "POST",
      url: $rootScope.url + "api/users/signin",
      data: $scope.data
    }).then(function (res) {
      localStorage.setItem('token', res.data.token);
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;
      setTimeout(function () {
        $state.go('home');
      }, 500)
    }).catch(function (res) {
      console.log(res.data)
    })
  }
}

function registerController($scope, $http) {

}

function izvestajiController($scope, $http) {

}

function statistikaController($scope, $rootScope, $http, $stateParams, $state, $ngConfirm) {

  $scope.datePicker = {startDate: null, endDate: null};
  $scope.datePickerOptions = {
    locale: {
      applyLabel: "Potvrdi",
      fromLabel: "Od",
      format: "YYYY-MM-DD", //will give you 2017-01-06
      //format: "D-MMM-YY", //will give you 6-Jan-17
      //format: "D-MMMM-YY", //will give you 6-January-17
      toLabel: "Do",
      cancelLabel: 'Odustani',
      // customRangeLabel: 'Odaberi (Od - Do)'
    }
  }

  $.datepicker.regional['sr'] = {
    closeText: 'Затвори',
    prevText: '&#x3c;',
    nextText: '&#x3e;',
    currentText: 'Данас',
    monthNames: ['Јануар', 'Фебруар', 'Март', 'Април', 'Мај', 'Јун',
      'Јул', 'Август', 'Септембар', 'Октобар', 'Новембар', 'Децембар'],
    monthNamesShort: ['Јан', 'Феб', 'Мар', 'Апр', 'Мај', 'Јун',
      'Јул', 'Авг', 'Сеп', 'Окт', 'Нов', 'Дец'],
    dayNames: ['Недеља', 'Понедељак', 'Уторак', 'Среда', 'Четвртак', 'Петак', 'Субота'],
    dayNamesShort: ['Нед', 'Пон', 'Уто', 'Сре', 'Чет', 'Пет', 'Суб'],
    dayNamesMin: ['Не', 'По', 'Ут', 'Ср', 'Че', 'Пе', 'Су'],
    weekHeader: 'Сед',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['sr']);
  $('#datumIzvrsenja').datepicker();
  $scope.dodajZadatak = function () {
    $scope.post = true;
    $scope.put = false;
    $scope.customZadatak = {
      user_id: $stateParams.id,
      task_description: "",
      task_date: "",
      task_time: ""
    };
    $('.dodajZadatakDiv').css('display', 'block');
  }

  $scope.customZadatak = {
    user_id: $stateParams.id,
    task_description: "",
    task_date: "",
    task_time: ""
  };

  $scope.logDetails = function (orderId, userId) {
    $('.logStatistic').css({
      'visibility': 'visible',
      'opacity': '1'
    });

    $http({
      method: "GET",
      url: $rootScope.url + 'api/user-timelogs-by-order/' + userId + "/" + orderId
    }).then(function (res) {
      $scope.vremena = res.data;
      console.log($scope.vremena);
      for (var i = 0; i < res.data.orderTimeLogs.length; i++) {

        $scope.vremena.orderTimeLogs[i].created_at = new Date($scope.vremena.orderTimeLogs[i].created_at);

        var startTime = new Date(res.data.orderTimeLogs[i].start_time);
        var endTime = new Date(res.data.orderTimeLogs[i].end_time);
        var offset = (startTime.getTimezoneOffset() / 60) * -1;

        var startTimeHours = startTime.getHours() + offset;
        var endTimeHours = endTime.getHours() + offset;

        $scope.vremena.orderTimeLogs[i].start_vreme = startTime;
        $scope.vremena.orderTimeLogs[i].startHour = startTimeHours;
        $scope.vremena.orderTimeLogs[i].endHour = endTimeHours;
        $scope.vremena.orderTimeLogs[i].end_vreme = endTime;
      }
    })
  };

  $scope.closeDiv = function () {
    $('.logStatistic').css({
      'visibility': 'hidden',
      'opacity': '0'
    });
  }

  $scope.post = true;
  $scope.put = false;
  $scope.editTask = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.taskId = id;
    $('.dodajZadatakDiv').css('display', 'block');
    $http({
      method: "GET",
      url: $rootScope.url + "api/tasks/" + id
    }).then(function (res) {
      var date = new Date(res.data.task_date);
      $scope.datumIzvrsenja = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      $scope.customZadatak = {
        task_description: res.data.task_description,
        task_date: $scope.datumIzvrsenja,
        task_time: res.data.task_time / 60
      };
    })
  }

  $scope.deleteTask = function (id) {
    $ngConfirm({
      title: 'Brisanje zadatka',
      content: 'Da li ste sigurni da želite obrišete ovaj zadatak?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/tasks/" + id
            }).then(function () {
              $state.reload();
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.dodajZadatakRadniku = function () {
    if ($scope.post) {
      $scope.customZadatak.task_date = $scope.datumIzvrsenja;
      $scope.customZadatak.task_time = $scope.customZadatak.task_time * 60;
      $http({
        method: "POST",
        url: $rootScope.url + "api/tasks",
        data: $scope.customZadatak
      }).then(function () {
        $state.reload();
      })
    } else {
      $scope.customZadatak.task_date = $scope.datumIzvrsenja;
      $scope.customZadatak.task_time = $scope.customZadatak.task_time * 60;
      $http({
        method: "PUT",
        url: $rootScope.url + "api/tasks/" + $scope.taskId,
        data: $scope.customZadatak
      }).then(function () {
        $state.reload();
      })
    }

  }

  $scope.odustani = function () {
    $('.dodajZadatakDiv').css('display', 'none');
    $scope.datumIzvrsenja = "";
    $scope.customZadatak = {
      task_description: "",
      task_date: "",
      task_time: ""
    };
  }


  $scope.$watch('datePicker', function (res) {
    if (res.startDate != undefined && res.endDate != undefined) {

      var start = res.startDate._d;
      var end = res.endDate._d;

      var startDate = start.getFullYear() + "-" + (start.getMonth() + 1) + "-" + start.getDate();
      var endDate = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate();

      $scope.fromDay = start.getDate() + "." + (start.getMonth() + 1) + "." + start.getFullYear();
      $scope.toDay = end.getDate() + "." + (end.getMonth() + 1) + "." + end.getFullYear() + ".";

      $http({
        method: "GET",
        url: $rootScope.url + "api/user-statistics-filtered/" + $stateParams.id + "/" + startDate + "/" + endDate
      }).then(function (res) {
        $scope.statistika = res.data;


        function sum(num1, num2) {
          return num1 + num2;
        }

        var ukupnoNormirano = [];
        var ukupnoUtroseno = [];

        if (res.data.time_data.length == 0) {
          $scope.empty = true;
        } else {
          $scope.empty = false;
        }
        for (var n = 0; n < res.data.tasks.length; n++) {
          // Utroseno za posebne zadatke

          var logHoursTask = Math.floor($scope.statistika.tasks[n].task_time / 3600);
          var logMinutesTask = Math.floor($scope.statistika.tasks[n].task_time % 3600 / 60);
          var logSecondsTask = Math.floor($scope.statistika.tasks[n].task_time % 3600 % 60);
          $scope.statistika.tasks[n].logH = logHoursTask;
          $scope.statistika.tasks[n].logM = logMinutesTask;
          $scope.statistika.tasks[n].logS = logSecondsTask;

          ukupnoUtroseno.push(parseInt($scope.statistika.tasks[n].task_time));

        }

        for (var i = 0; i < res.data.time_data.length; i++) {

          // Utroseno za naloge
          var logHours = Math.floor($scope.statistika.time_data[i].sum_time / 3600);
          var logMinutes = Math.floor($scope.statistika.time_data[i].sum_time % 3600 / 60);
          var logSeconds = Math.floor($scope.statistika.time_data[i].sum_time % 3600 % 60);
          $scope.statistika.time_data[i].logH = logHours;
          $scope.statistika.time_data[i].logM = logMinutes;
          $scope.statistika.time_data[i].logS = logSeconds;

          ukupnoUtroseno.push(parseInt($scope.statistika.time_data[i].sum_time));

          if ($scope.statistika.time_data[i].logM < 10) {
            $scope.statistika.time_data[i].logM = "0" + logMinutes;
          }

          if ($scope.statistika.time_data[i].logS < 10) {
            $scope.statistika.time_data[i].logS = "0" + logSeconds;
          }

          if ($scope.statistika.time_data[i].logH < 10) {
            $scope.statistika.time_data[i].logH = "0" + logHours;
          }

          if ($scope.statistika.user.sector_id == 3) {
            var hours = Math.floor($scope.statistika.time_data[i].krojenje_vreme / 60);
            var minutes = $scope.statistika.time_data[i].krojenje_vreme % 60;

            $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].krojenje_state;

            $scope.statistika.time_data[i].norHours = hours;
            $scope.statistika.time_data[i].norMinutes = minutes;

            ukupnoNormirano.push($scope.statistika.time_data[i].krojenje_vreme)

          } else if ($scope.statistika.user.sector_id == 4) {
            var hours = Math.floor($scope.statistika.time_data[i].sivenje_vreme / 60);
            var minutes = $scope.statistika.time_data[i].sivenje_vreme % 60;

            $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].sivenje_state;

            $scope.statistika.time_data[i].norHours = hours;
            $scope.statistika.time_data[i].norMinutes = minutes;

            ukupnoNormirano.push($scope.statistika.time_data[i].sivenje_vreme)

          } else if ($scope.statistika.user.sector_id == 5) {
            var hours = Math.floor($scope.statistika.time_data[i].tapaciranje_vreme / 60);
            var minutes = $scope.statistika.time_data[i].tapaciranje_vreme % 60;

            $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].tapaciranje_state;

            $scope.statistika.time_data[i].norHours = hours;
            $scope.statistika.time_data[i].norMinutes = minutes;

            ukupnoNormirano.push($scope.statistika.time_data[i].tapaciranje_vreme)

          } else if ($scope.statistika.user.sector_id == 6) {
            var hours = Math.floor($scope.statistika.time_data[i].priprema_vreme / 60);
            var minutes = $scope.statistika.time_data[i].priprema_vreme % 60;

            $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].priprema_state;

            $scope.statistika.time_data[i].norHours = hours;
            $scope.statistika.time_data[i].norMinutes = minutes;

            ukupnoNormirano.push($scope.statistika.time_data[i].priprema_vreme)

          }

          if ($scope.statistika.time_data[i].logH <= $scope.statistika.time_data[i].norHours && $scope.statistika.time_data[i].logM <= $scope.statistika.time_data[i].norMinutes) {
            $scope.statistika.time_data[i].green = true;
            $scope.statistika.time_data[i].red = false;
          } else {
            $scope.statistika.time_data[i].green = false;
            $scope.statistika.time_data[i].red = true;
          }

        }

        // Ukupno normirano vreme ////
        if (ukupnoNormirano.length != 0) {
          var normiranoSum = ukupnoNormirano.reduce(sum);
          $scope.ukupnoNormiranoH = Math.floor(normiranoSum / 60);
          $scope.ukupnoNormiranoM = normiranoSum % 60;
        }

        // Ukupno utroseno vreme ////

        if (ukupnoUtroseno.length != 0) {
          var utrosenoSum = ukupnoUtroseno.reduce(sum);
          var logSumHours = Math.floor(utrosenoSum / 3600);
          var logSumMinutes = Math.floor(utrosenoSum % 3600 / 60);
          var logSumSeconds = Math.floor(utrosenoSum % 3600 % 60);

          $scope.utrosenoH = logSumHours;
          $scope.utrosenoM = logSumMinutes;
          $scope.utrosenoS = logSumSeconds;
        }
        $scope.sek = $scope.sektor[res.data.user.sector_id - 1].sector;
      }).catch(function (res) {
        if (res.data.error == "token_not_provided") {
          $state.go('login')
        } else if (res.data.error == 'token_expired') {
          $state.go('login')
        }
      });
    }
  });

  $scope.sektor = [
    {sector: "Neraspoređen"},
    {sector: "Administracija"},
    {sector: "Krojenje"},
    {sector: "Šivenje"},
    {sector: "Tapaciranje"},
    {sector: "Priprema"},
  ]

  $http({
    method: "GET",
    url: $rootScope.url + "api/user-statistics/" + $stateParams.id + "/0"
  }).then(function (res) {
    $scope.statistika = res.data;

    function sum(num1, num2) {
      return num1 + num2;
    }

    $scope.startedOrder = res.data.started_order;

    var ukupnoNormirano = [];
    var ukupnoUtroseno = [];

    if (res.data.time_data.length == 0) {
      $scope.empty = true;
    } else {
      $scope.empty = false;
    }

    for (var n = 0; n < res.data.tasks.length; n++) {
      // Utroseno za posebne zadatke

      var logHoursTask = Math.floor($scope.statistika.tasks[n].task_time / 3600);
      var logMinutesTask = Math.floor($scope.statistika.tasks[n].task_time % 3600 / 60);
      $scope.statistika.tasks[n].logH = logHoursTask;
      $scope.statistika.tasks[n].logM = logMinutesTask;

      ukupnoUtroseno.push(parseInt($scope.statistika.tasks[n].task_time));

    }

    for (var i = 0; i < res.data.time_data.length; i++) {

      var logHours = Math.floor($scope.statistika.time_data[i].sum_time / 3600);
      var logMinutes = Math.floor($scope.statistika.time_data[i].sum_time % 3600 / 60);
      var logSeconds = Math.floor($scope.statistika.time_data[i].sum_time % 3600 % 60);
      $scope.statistika.time_data[i].logH = logHours;
      $scope.statistika.time_data[i].logM = logMinutes;
      $scope.statistika.time_data[i].logS = logSeconds;

      // $scope.statistika.time_data[i].startedWorkOnOrder = false;
      // if ($scope.statistika.time_data[i].logS > 0 == false) {
      //   $scope.statistika.time_data[i].startedWorkOnOrder = true;
      // }

      ukupnoUtroseno.push(parseInt($scope.statistika.time_data[i].sum_time));


      if ($scope.statistika.time_data[i].logM < 10) {
        $scope.statistika.time_data[i].logM = "0" + logMinutes;
      }

      if ($scope.statistika.time_data[i].logS < 10) {
        $scope.statistika.time_data[i].logS = "0" + logSeconds;
      }

      if ($scope.statistika.time_data[i].logH < 10) {
        $scope.statistika.time_data[i].logH = "0" + logHours;
      }

      if ($scope.statistika.user.sector_id == 3) {
        var hours = Math.floor($scope.statistika.time_data[i].krojenje_vreme / 60);
        var minutes = $scope.statistika.time_data[i].krojenje_vreme % 60;

        $scope.statistika.time_data[i].norHours = hours;
        $scope.statistika.time_data[i].norMinutes = minutes;

        $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].krojenje_state;

        ukupnoNormirano.push($scope.statistika.time_data[i].krojenje_vreme)

      } else if ($scope.statistika.user.sector_id == 4) {
        var hours = Math.floor($scope.statistika.time_data[i].sivenje_vreme / 60);
        var minutes = $scope.statistika.time_data[i].sivenje_vreme % 60;

        $scope.statistika.time_data[i].norHours = hours;
        $scope.statistika.time_data[i].norMinutes = minutes;

        $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].sivenje_state;

        ukupnoNormirano.push($scope.statistika.time_data[i].sivenje_vreme)

      } else if ($scope.statistika.user.sector_id == 5) {
        var hours = Math.floor($scope.statistika.time_data[i].tapaciranje_vreme / 60);
        var minutes = $scope.statistika.time_data[i].tapaciranje_vreme % 60;

        $scope.statistika.time_data[i].norHours = hours;
        $scope.statistika.time_data[i].norMinutes = minutes;

        $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].tapaciranje_state;

        ukupnoNormirano.push($scope.statistika.time_data[i].tapaciranje_vreme)

      } else if ($scope.statistika.user.sector_id == 6) {
        var hours = Math.floor($scope.statistika.time_data[i].priprema_vreme / 60);
        var minutes = $scope.statistika.time_data[i].priprema_vreme % 60;

        $scope.statistika.time_data[i].norHours = hours;
        $scope.statistika.time_data[i].norMinutes = minutes;

        $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].priprema_state;


        ukupnoNormirano.push($scope.statistika.time_data[i].priprema_vreme)

      }

      if ($scope.statistika.time_data[i].logH <= $scope.statistika.time_data[i].norHours && $scope.statistika.time_data[i].logM <= $scope.statistika.time_data[i].norMinutes) {
        $scope.statistika.time_data[i].green = true;
        $scope.statistika.time_data[i].red = false;
      } else {
        $scope.statistika.time_data[i].green = false;
        $scope.statistika.time_data[i].red = true;
      }

    }

    // Ukupno normirano vreme ////
    if (ukupnoNormirano.length != 0) {
      var normiranoSum = ukupnoNormirano.reduce(sum);
      $scope.ukupnoNormiranoH = Math.floor(normiranoSum / 60);
      $scope.ukupnoNormiranoM = normiranoSum % 60;
    }
    // Ukupno utroseno vreme ////

    if (ukupnoUtroseno.length != 0) {
      var utrosenoSum = ukupnoUtroseno.reduce(sum);
      var logSumHours = Math.floor(utrosenoSum / 3600);
      var logSumMinutes = Math.floor(utrosenoSum % 3600 / 60);
      var logSumSeconds = Math.floor(utrosenoSum % 3600 % 60);

      $scope.utrosenoH = logSumHours;
      $scope.utrosenoM = logSumMinutes;
      $scope.utrosenoS = logSumSeconds;
    }
    console.log(res.data)
    $scope.sek = $scope.sektor[res.data.user.sector_id - 1].sector;
  })


  $scope.statisticType = "Dnevna statistika";
  $scope.filter = function (days) {
    $scope.datePicker = {startDate: null, endDate: null};
    $scope.fromDay = undefined;
    $scope.toDay = undefined;

    if (days == '0') {
      $scope.statisticType = "Dnevna statistika";
    } else if (days == '7') {
      $scope.statisticType = "Nedeljna Statistika";
    } else if (days == '30') {
      $scope.statisticType = "Mesecna statistika";
    } else if (days == '90') {
      $scope.statisticType = "Statistika za zadnja tri meseca";
    }
    $http({
      method: "GET",
      url: $rootScope.url + "api/user-statistics/" + $stateParams.id + "/" + days
    }).then(function (res) {

      $scope.statistika = res.data;

      $scope.startedOrder = res.data.started_order;


      function sum(num1, num2) {
        return num1 + num2;
      }

      var ukupnoNormirano = [];
      var ukupnoUtroseno = [];

      if (res.data.time_data.length == 0) {
        $scope.empty = true;
      } else {
        $scope.empty = false;
      }

      for (var n = 0; n < res.data.tasks.length; n++) {
        // Utroseno za posebne zadatke

        var logHoursTask = Math.floor($scope.statistika.tasks[n].task_time / 3600);
        var logMinutesTask = Math.floor($scope.statistika.tasks[n].task_time % 3600 / 60);
        $scope.statistika.tasks[n].logH = logHoursTask;
        $scope.statistika.tasks[n].logM = logMinutesTask;

        ukupnoUtroseno.push(parseInt($scope.statistika.tasks[n].task_time));

      }

      for (var i = 0; i < res.data.time_data.length; i++) {

        var logHours = Math.floor($scope.statistika.time_data[i].sum_time / 3600);
        var logMinutes = Math.floor($scope.statistika.time_data[i].sum_time % 3600 / 60);
        var logSeconds = Math.floor($scope.statistika.time_data[i].sum_time % 3600 % 60);
        $scope.statistika.time_data[i].logH = logHours;
        $scope.statistika.time_data[i].logM = logMinutes;
        $scope.statistika.time_data[i].logS = logSeconds;

        ukupnoUtroseno.push(parseInt($scope.statistika.time_data[i].sum_time));

        if ($scope.statistika.time_data[i].logM < 10) {
          $scope.statistika.time_data[i].logM = "0" + logMinutes;
        }

        if ($scope.statistika.time_data[i].logS < 10) {
          $scope.statistika.time_data[i].logS = "0" + logSeconds;
        }

        if ($scope.statistika.time_data[i].logH < 10) {
          $scope.statistika.time_data[i].logH = "0" + logHours;
        }

        if ($scope.statistika.user.sector_id == 3) {
          var hours = Math.floor($scope.statistika.time_data[i].krojenje_vreme / 60);
          var minutes = $scope.statistika.time_data[i].krojenje_vreme % 60;

          $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].krojenje_state;
          $scope.statistika.time_data[i].norHours = hours;
          $scope.statistika.time_data[i].norMinutes = minutes;

          ukupnoNormirano.push($scope.statistika.time_data[i].krojenje_vreme)

        } else if ($scope.statistika.user.sector_id == 4) {
          var hours = Math.floor($scope.statistika.time_data[i].sivenje_vreme / 60);
          var minutes = $scope.statistika.time_data[i].sivenje_vreme % 60;

          $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].sivenje_state;

          $scope.statistika.time_data[i].norHours = hours;
          $scope.statistika.time_data[i].norMinutes = minutes;

          ukupnoNormirano.push($scope.statistika.time_data[i].sivenje_vreme)

        } else if ($scope.statistika.user.sector_id == 5) {
          var hours = Math.floor($scope.statistika.time_data[i].tapaciranje_vreme / 60);
          var minutes = $scope.statistika.time_data[i].tapaciranje_vreme % 60;

          $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].tapaciranje_state;

          $scope.statistika.time_data[i].norHours = hours;
          $scope.statistika.time_data[i].norMinutes = minutes;

          ukupnoNormirano.push($scope.statistika.time_data[i].tapaciranje_vreme)

        } else if ($scope.statistika.user.sector_id == 6) {
          var hours = Math.floor($scope.statistika.time_data[i].priprema_vreme / 60);
          var minutes = $scope.statistika.time_data[i].priprema_vreme % 60;

          $scope.statistika.time_data[i].status = $scope.statistika.time_data[i].priprema_state;

          $scope.statistika.time_data[i].norHours = hours;
          $scope.statistika.time_data[i].norMinutes = minutes;

          ukupnoNormirano.push($scope.statistika.time_data[i].priprema_vreme)
        }

        if ($scope.statistika.time_data[i].logH <= $scope.statistika.time_data[i].norHours && $scope.statistika.time_data[i].logM <= $scope.statistika.time_data[i].norMinutes) {
          $scope.statistika.time_data[i].green = true;
          $scope.statistika.time_data[i].red = false;
        } else {
          $scope.statistika.time_data[i].green = false;
          $scope.statistika.time_data[i].red = true;
        }

      }

      // Ukupno normirano vreme ////
      if (ukupnoNormirano.length != 0) {
        var normiranoSum = ukupnoNormirano.reduce(sum);
        $scope.ukupnoNormiranoH = Math.floor(normiranoSum / 60);
        $scope.ukupnoNormiranoM = normiranoSum % 60;
      }

      // Ukupno utroseno vreme ////

      if (ukupnoUtroseno.length != 0) {
        var utrosenoSum = ukupnoUtroseno.reduce(sum);
        var logSumHours = Math.floor(utrosenoSum / 3600);
        var logSumMinutes = Math.floor(utrosenoSum % 3600 / 60);
        var logSumSeconds = Math.floor(utrosenoSum % 3600 % 60);

        $scope.utrosenoH = logSumHours;
        $scope.utrosenoM = logSumMinutes;
        $scope.utrosenoS = logSumSeconds;
      }
      $scope.sek = $scope.sektor[res.data.user.sector_id - 1].sector;
    })
  }

}

function recentShipmentsController($scope, $http, $rootScope) {
  $scope.getRecent = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/first-shipments-4-delivery"
    }).then(function (res) {
      $scope.pravac = res.data;
    })
  };
  $scope.getRecent();

  setInterval(function () {
    $scope.getRecent();
  }, 30000)

}

function arhiviraniPravciController($scope, $http, $rootScope, $ngConfirm) {
  $scope.getArchiveShipments = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/shipments-archived"
    }).then(function (res) {
      $scope.shipments = res.data.shipments;
      for (var i = 0; i < $scope.shipments.length; i++) {
        $scope.shipments[i].datum_slanja = new Date($scope.shipments[i].datum_slanja);
      }
    })
  }
  $scope.getArchiveShipments();

  $scope.unarchive = function (id) {
    $ngConfirm({
      title: 'Dearhiviranje pravca',
      content: 'Da li ste sigurni da želite dearhivirate ovaj pravac?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "GET",
              url: $rootScope.url + "api/shipment-reactivate/" + id
            }).then(function () {
              $scope.getArchiveShipments();
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }
}

function dodajKategorijuController($scope, $http, $rootScope, $ngConfirm) {
  $scope.kategorija = {
    naziv_kategorije: ""
  };

  $scope.getCategories = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/category"
    }).then(function (res) {
      console.log(res.data)
      $scope.categories = res.data.categories;
    })
  }
  $scope.getCategories();

  $scope.post = true;
  $scope.put = false;

  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.catId = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/category/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      $scope.kategorija = {
        naziv_kategorije: res.data.category.naziv_kategorije
      }
    })
  }

  $scope.dodajKategoriju = function () {
    if ($scope.post) {
      $http({
        method: "POST",
        url: $rootScope.url + "api/category",
        data: $scope.kategorija
      }).then(function () {
        $scope.getCategories();
      })
    } else {
      $http({
        method: "PUT",
        url: $rootScope.url + "api/category/" + $scope.catId,
        data: $scope.kategorija
      }).then(function () {
        $scope.getCategories();
      })
    }
  }

  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje kategorije',
      content: 'Da li ste sigurni da želite obrisati kategoriju?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/category/" + id
            }).then(function () {
              $scope.getCategories();
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

}

function porudzbineController($scope, $rootScope, $state, $http, $ngConfirm) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/weborders"
  }).then(function (res) {
    console.log(res.data);
    $scope.weborders = res.data.weborders;
    for (var i = 0; i < res.data.weborders.length; i++) {
      $scope.weborders[i].created_at = new Date(res.data.weborders[i].created_at);
    }
  })

  // Delete
  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje porudzbine',
      content: 'Da li ste sigurni da želite obrisati porduzbinu?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/weborders/" + id
            }).then(function () {
              $state.reload();
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.pretraga = {
    porudzbina: ""
  }

  // $scope.$watch('pretraga.porudzbina', function(res) {
  //   if (res != '') {
  //     $http({
  //       method: "POST",
  //       url: $rootScope.url + "api/weborders-filtered",
  //       data: {pretraga: res}
  //     }).then(function(response) {
  //       $scope.weborders = response.data.weborder;
  //       console.log($scope.weborders)
  //       for (var i = 0; i < response.data.weborderlength; i++) {
  //         $scope.weborders[i].created_at = new Date(response.data.weborder[i].created_at);
  //       }
  //     })
  //   }
  // })
}

function badgeController($rootScope, $http, $scope) {
  // Get badge number
  $http({
    method: "GET",
    url: $rootScope.url + "api/unprocessed-count"
  }).then(function (res) {
    $scope.badge = res.data;
  })
}

function porudzbinaController($scope, $http, $state, $rootScope, $stateParams, $ngConfirm) {
  $http({
    method: "GET",
    url: $rootScope.url + "api/weborders/" + $stateParams.id
  }).then(function (res) {
    console.log(res.data);
    $scope.weborder = res.data.weborder[0];
    $scope.weborder.created_at = new Date($scope.weborder.created_at);
  })

  $scope.processOrder = function (porudzbinaId) {
    $http({
      method: "GET",
      url: $rootScope.url + "api/process-weborder/" + porudzbinaId
    }).then(function () {
      $state.go('app.kreiranjePorudzbine', {id: porudzbinaId});
    })
  }

  // Delete
  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje porudzbine',
      content: 'Da li ste sigurni da želite obrisati porduzbinu?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/weborders/" + id
            }).then(function () {
              $state.go('app.porudzbine');
            });
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }
}

function porudzbinaKreiranjeController($scope, $http, $stateParams, $state, $rootScope) {
  $scope.getOrder = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/weborders/" + $stateParams.id
    }).then(function (res) {
      $scope.nalog = {
        item_id: res.data.weborder[0].item_id,
        proizvod: res.data.weborder[0].item.naziv_proizvoda,
        client: res.data.weborder[0].client.ime,
        client_id: res.data.weborder[0].client_id,
        kolicina: res.data.weborder[0].kolicina,
        napomena_javna: res.data.weborder[0].napomena_korisnika,
        obracunati_rabat: res.data.weborder[0].obracunati_rabat,
        adresa_isporuke: res.data.weborder[0].client.adresa,
        mesto_isporuke: res.data.weborder[0].client.mesto,
        ukrasni_step: res.data.weborder[0].ukrasni_step,
        napomena_privatna: res.data.weborder[0].napomena_privatna,
        parts: ""
      };

      $scope.mestoIsporuke = [];
      $scope.mestoIsporuke.push({
        adresa: $scope.nalog.adresa_isporuke,
        mesto: $scope.nalog.mesto_isporuke
      })

      $http({
        method: "GET",
        url: $rootScope.url + "api/clients/" + $scope.nalog.client_id
      }).then(function (res) {
        $scope.mestoIsporuke = res.data;
        if ($scope.mestoIsporuke.mesto_isporuke.length == 0) {
          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: res.data.adresa,
            mesto: res.data.mesto
          })
          // $scope.mestoIsporuke.mesto_isporuke.push({
          //   adresa: $scope.mestoIsporuke.adresa,
          //   mesto: $scope.mestoIsporuke.mesto
          // })
          // $scope.nalog.mesto_isporuke = $scope.mestoIsporuke.mesto_isporuke[0].mesto;
          // $scope.nalog.adresa_isporuke = $scope.mestoIsporuke.mesto_isporuke[0].adresa;
        } else {

          $scope.mestoIsporuke.mesto_isporuke.push({
            adresa: res.data.adresa,
            mesto: res.data.mesto
          })
        }
      })


      $scope.nalog.parts = [];
      $scope.deo = [];
      for (var i = 0; i < res.data.weborder[0].parts.length; i++) {
        $scope.deo.push({
          part_naziv: res.data.weborder[0].parts[i].naziv,
          part_id: res.data.weborder[0].parts[i].pivot.part_id,
          material: res.data.weborder[0].parts[i].pivot.material
        });
      }
      $scope.getMaterials();
      $scope.nalog.parts.push($scope.deo);
    })
  };
  $scope.getOrder();

  $scope.getMaterials = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/materials"
    }).then(function (res) {
      $scope.materials = res.data;
    })
  }
  $scope.getMaterials();

  $scope.materijali = {
    tags: true
  };

  $scope.revert = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/unprocess-weborder/" + $stateParams.id
    }).then(function () {
      $state.go('app.porudzbine');
    })
  }

  $scope.cancel = function () {
    $state.go('home');
  }

  $scope.kupci = {
    tags: true
  };


  $scope.$watch('nalog.brojKomada', function (res) {
    if (res <= 1) {
      $scope.nalog.brojKomada = 1;
    }
  })

  $scope.adresaIsporuke = undefined;
  $scope.$watch('adresaIsporuke', function (res) {
    if (res != undefined) {
      var response = JSON.parse(res);
      $scope.nalog.mesto_isporuke = response.mesto;
      $scope.nalog.adresa_isporuke = response.adresa;
    }
  });

  $scope.dodajNalog = function () {
    $scope.nalog.parts = $scope.deo;

    // for (var i = 0; i < $scope.nalog.parts.length; i++) {
    //   if (typeof $scope.nalog.parts[i].material_id != 'number') {
    //     var parse = JSON.parse($scope.nalog.parts[i].material_id);
    //     $scope.nalog.parts[i].material_id = parse.id;
    //     $scope.nalog.parts[i].material = parse.naziv_materijala;
    //   }
    // }
    $http({
      method: "POST",
      url: $rootScope.url + "api/orders",
      data: $scope.nalog
    }).then(function () {
      $state.go('home');
    })
  }
}

function webUserController($scope, $http, $rootScope, $state, $ngConfirm) {
  $scope.getClients = function () {
    $http({
      method: "GET",
      url: $rootScope.url + "api/clients"
    }).then(function (res) {
      $scope.clients = res.data;
    })
  }
  $scope.getClients();

  $scope.korisnik = {
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    sector_id: "",
    role_id: 3,
    sector_id: "",
    client_id: ""
  };

  $scope.closePop = function (popType) {
    $('.' + popType).css('display', 'none');
  }

  $scope.reload = function() {
    $state.reload();
  }

  $scope.showPass = function (qr, name, lastname) {
    // var decoded = atob(qr);

    var auth = decoded;
    var index = auth.indexOf(" ");
    var pass = auth.slice(index + 1, auth.length).trim();

    $scope.pass = pass;
    // $scope.name = name;
    // $scope.surname = lastname;
    $('.popUpDiv2').css('display', 'flex');
  }
  $scope.post = true;
  $scope.put = false;
  var admin;
  $scope.edit = function (id) {
    $scope.post = false;
    $scope.put = true;
    $scope.id = id;
    $http({
      method: "GET",
      url: $rootScope.url + "api/users/" + id
    }).then(function (res) {
      window.scrollTo(0, 0);
      if (res.data.administrator == 0) {
        admin = false;
      } else {
        admin = true;
      }
      var decoded = atob(res.data.qr);
      var auth = decoded;
      var index = auth.indexOf(" ");
      var pass = auth.slice(index + 1, auth.length).trim();
      $scope.pass = pass;
      $scope.korisnik = {
        first_name: res.data.first_name,
        last_name: res.data.last_name,
        email: res.data.email,
        administrator: admin,
        password: pass,
        sector_id: res.data.sector_id,
        client_id: res.data.client_id
      }
    })
  };
  $scope.dodajKorisnika = function () {
    if ($scope.post) {
      if ($scope.korisnik.sector_id == "") {
        $scope.korisnik.sector_id = 1;
      }
      $scope.korisnik.qr = btoa($scope.korisnik.email + " " + $scope.korisnik.password);
      $http({
        method: "POST",
        url: $rootScope.url + "api/users",
        data: $scope.korisnik
      }).then(function () {
        $state.reload();
      })
    } else {
      $scope.korisnik.qr = btoa($scope.korisnik.email + " " + $scope.korisnik.password);
      $http({
        method: "PUT",
        url: $rootScope.url + "api/users/" + $scope.id,
        data: $scope.korisnik
      }).then(function () {
        $state.reload();
      })
    }
  };
  $scope.delete = function (id) {
    $ngConfirm({
      title: 'Brisanje korisnika',
      content: 'Da li ste sigurni da želite obrisati korisnika?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        Da: {
          text: 'Da',
          btnClass: 'btn-red',
          action: function (scope, button) {
            $http({
              method: "DELETE",
              url: $rootScope.url + "api/users/" + id
            }).then(function () {
              $state.reload();
            })
          }
        },
        Ne: {
          text: "Ne",
          btnClass: 'btn-blue',
          action: function () {
          }
        }
      }
    })
  }

  $scope.getWebUsers = function() {
    $http({
      method: "GET",
      url: $rootScope.url + "api/webusers"
    }).then(function(res) {
      console.log(res.data);
      $scope.users = res.data;
    })
  }
  $scope.getWebUsers();
}


angular
  .module('inspinia')
  .run(function ($rootScope, $http, $state, $location, $window) {
    $rootScope.url = "http://staff2-api.ippcentar.com/";
    if (localStorage.getItem('token') != null) {
      $rootScope.token = localStorage.getItem('token');
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
    }
    $rootScope.$on('$locationChangeSuccess', function () {
      if (localStorage.getItem('token') != null) {
        $rootScope.token = localStorage.getItem('token');
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
      }
    })
  })

  .controller('MainCtrl', MainCtrl)
  .controller('dashboardFlotOne', dashboardFlotOne)
  .controller('dashboardFlotTwo', dashboardFlotTwo)
  .controller('dashboardFive', dashboardFive)
  // .controller('dashboardMap', dashboardMap)
  // .controller('flotChartCtrl', flotChartCtrl)
  // .controller('rickshawChartCtrl', rickshawChartCtrl)
  // .controller('sparklineChartCtrl', sparklineChartCtrl)
  // .controller('widgetFlotChart', widgetFlotChart)
  // .controller('modalDemoCtrl', modalDemoCtrl)
  // .controller('ionSlider', ionSlider)
  // .controller('wizardCtrl', wizardCtrl)
  // .controller('CalendarCtrl', CalendarCtrl)
  // .controller('chartJsCtrl', chartJsCtrl)
  // .controller('GoogleMaps', GoogleMaps)
  .controller('ngGridCtrl', ngGridCtrl)
  // .controller('codeEditorCtrl', codeEditorCtrl)
  .controller('nestableCtrl', nestableCtrl)
  // .controller('notifyCtrl', notifyCtrl)
  .controller('translateCtrl', translateCtrl)
  // .controller('imageCrop', imageCrop)
  // .controller('diff', diff)
  // .controller('idleTimer', idleTimer)
  // .controller('liveFavicon', liveFavicon)
  // .controller('formValidation', formValidation)
  // .controller('agileBoard', agileBoard)
  // .controller('draggablePanels', draggablePanels)
  // .controller('chartistCtrl', chartistCtrl)
  // .controller('metricsCtrl', metricsCtrl)
  .controller('sweetAlertCtrl', sweetAlertCtrl)
  // .controller('selectCtrl', selectCtrl)
  .controller('toastrCtrl', toastrCtrl)
  // .controller('loadingCtrl', loadingCtrl)
  // .controller('datatablesCtrl', datatablesCtrl)
  // .controller('truncateCtrl', truncateCtrl)
  // .controller('touchspinCtrl', touchspinCtrl)
  // .controller('tourCtrl', tourCtrl)
  // .controller('jstreeCtrl', jstreeCtrl)

  //Print
  .controller('printNalogController', printNalogController)
  .controller('printNalogPripController', printNalogPripController)
  .controller('printShipmentController', printShipmentController)
  .controller('printAdresnaNalController', printAdresnaNalController)
  .controller('izvoznaFakturaController', izvoznaFakturaController)
  .controller('printFakturaController', printFakturaController)
  .controller('printGotovaFakturaController', printGotovaFakturaController)
  //

  // Faktura
  .controller('fakturaController', fakturaController)

  //auth
  .controller('loginController', loginController)
  .controller('registerController', registerController)

  .controller('dodajKupcaController', dodajKupcaController)
  .controller('azurirajKupcaController', azurirajKupcaController)
  .controller('dodajProizvodController', dodajProizvodController)
  .controller('deoProizvodaController', deoProizvodaController)
  .controller('materijalController', materijalController)
  .controller('nalogController', nalogController)
  .controller('planerController', planerController)
  .controller('korisniciController', korisniciController)
  .controller('vozilaController', vozilaController)
  .controller('vozaciController', vozaciController)
  .controller('klijentController', klijentController)
  .controller('nalogUpdateController', nalogUpdateController)
  .controller('dodajPravacController', dodajPravacController)
  .controller('faktureController', faktureController)
  .controller('fakturaDetaljnoController', fakturaDetaljnoController)
  .controller('izvestajiController', izvestajiController)
  .controller('naloziController', naloziController)
  .controller('statistikaController', statistikaController)
  .controller('proizvodiListaController', proizvodiListaController)
  .controller('proizvodStatistikaController', proizvodStatistikaController)
  .controller('recentShipmentsController', recentShipmentsController)
  .controller('arhiviraniPravciController', arhiviraniPravciController)
  .controller('dodajKategorijuController', dodajKategorijuController)
  .controller('porudzbineController', porudzbineController)
  .controller('porudzbinaController', porudzbinaController)
  .controller('porudzbinaKreiranjeController', porudzbinaKreiranjeController)
  .controller('badgeController', badgeController)
  .controller('webUserController', webUserController)


  .directive('ngRightClick', function ($parse) {
    return function (scope, element, attrs) {
      var fn = $parse(attrs.ngRightClick);
      element.bind('contextmenu', function (event) {
        scope.$apply(function () {
          event.preventDefault();
          fn(scope, {$event: event});
        });
      });
    };
  });
