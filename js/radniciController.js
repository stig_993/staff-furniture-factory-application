

function statistikaController($scope) {
  $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  $scope.series = ['Series A', 'Series B'];

  $scope.data = [
    [0.30, 0.30, 1.30, 0.30, 0.30, 0.30, 0.30],
    [1, 2, 3, 4, 5, 6, 7]
  ];
}

function timerController($scope) {

}

function nalogScanController($scope, $state, $http, $rootScope) {
  $scope.nalog = {
    broj: ""
  };
  $scope.scanNalog = function() {
    $http({
      method: "GET",
      url: $rootScope.urlW + "api/takeover-order/" + $scope.nalog.broj + "/18"
    }).then(function(res) {
      if (res.data.success) {
        $state.go('nalogTimer', {order_id: $scope.nalog.broj});
      }
    });
  };
}

function nalogTimerController($scope, $http, $rootScope, $stateParams) {
  var counter = 0;
  var isPaused = true;

  $("#pause, #resume").hide();
  $("#days, #hours, #first-divider, #second-divider").hide();
  $("#start").show();

  var t = window.setInterval(function() {
    if (!isPaused) {
      counter++;
      var s = counter;
      convertSeconds(Math.floor(s));
    }
  }, 1000);

// Button Click Events
  $("#start").click(function () {
    $http({
      method: "GET",
      url: $rootScope.urlW + "api/start-work-on-order/" + $stateParams.order_id + "/18"
    }).then(function(res) {
      console.log(res)
    });
    startClock();
    $(this).hide();
    $("#pause").show();
    $("#reset, #stop").css("opacity", "1");
  });

  $scope.pauza = {
    user_id: '18',
    order_id: '112',
    sector_id: 3,
    pause_id: 1
  };

  $("#pause").click(function (){
    pauseClock();
    $http({
      method: "POST",
      url: $rootScope.urlW + "api/log-work-on-order",
      data: $scope.pauza
    }).then(function(res) {
      console.log(res)
    });
    $(this).hide();
    $("#resume").show();
  });

  $("#resume").click(function (){
    resumeClock();
    $(this).hide();
    $("#pause").show();
  });

  $("#reset").click(function (){
    resetClock();
    $("#resume").hide();
    $("#pause").show();
  });

  $("#stop").click(function () {
    var logTime = $('#hours').html() + ":" + $('#minutes').html();

    stopClock();
    $("#pause, #resume").hide();
    $("#start").show();
    $("#reset, #stop").css("opacity", "0");
  });

  function startClock() {
    isPaused = false;
  }
  function pauseClock() {
    isPaused = true;
  }
  function resumeClock() { isPaused = false; }
  function resetClock() {
    counter = 0;
    $("#days").html("00");
    $("#hours").html("00");
    $("#minutes").html("00");
    $("#seconds").html("00");
  }

  function stopClock() {
    resetClock();
    isPaused = true;
  }

  function convertSeconds(s) {
    var days = Math.floor(s / 86400)
    var hours = Math.floor((s % 86400) / 3600);
    var minutes = Math.floor(((s % 86400) % 3600) / 60);
    var seconds = ((s % 86400) % 3600) % 60;

    if (days		< 10) {days 	 = "0" + days}
    if (hours 	< 10) {hours 	 = "0" + hours;}
    if (minutes < 10) {minutes = "0" + minutes;}
    if (seconds < 10) {seconds = "0" + seconds;}

    $("#days").html(days);
    $("#hours").html(hours);
    $("#minutes").html(minutes);
    $("#seconds").html(seconds);

    if (days == 0 && hours == 0) {
      $("#days, #hours").hide();
      $("#first-divider, #second-divider").hide();
    } else if (days == 0) {
      $("#days").hide();
      $("#hours").show();
      $("#second-divider").show();
    } else {
      $("p, .divider").show();
    }
  }
}

angular
  .module('inspinia')
  .run(function ($rootScope, $http, $state, $location, $window) {
    $rootScope.urlW = "http://staff.local/";
    // if (localStorage.getItem('token') != null) {
    //   $rootScope.token = localStorage.getItem('token');
    //   $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
    // }
    // $rootScope.$on('$locationChangeSuccess', function () {
    //   if (localStorage.getItem('token') != null) {
    //     $rootScope.token = localStorage.getItem('token');
    //     $http.defaults.headers.common['Authorization'] = 'Bearer ' + $rootScope.token;
    //   }
    // })
  })

  .controller('statistikaController', statistikaController)
  .controller('statistikaController', statistikaController)
  .controller('nalogScanController', nalogScanController)
  .controller('nalogTimerController', nalogTimerController)
